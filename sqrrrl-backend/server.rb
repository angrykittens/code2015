require 'csv'
require 'active_hash'
require 'sinatra'
require 'redis-objects'
require 'connection_pool'
require 'dish'
require 'chronic'
require 'time_difference'
require 'obscenity'

# All the hacks
class User < Dish::Plate
    coerce :age, -> (value) { value.to_i }
    coerce :income, -> (value) { value.to_f }

    def self.find(id)
        r=RUser.find(id)
        RUser::FIELDS.each {|f|
            r.send(f)
        }
        _u = User.from_redis(r)
        _u.new = _u.name.nil?
        _u
    end
    def self.from_redis(redis_value)
        User.new(JSON.parse(redis_value.to_json))
    end
end

class RUser
    attr_reader :vendor_id
    alias :id :vendor_id

    include Redis::Objects

    def initialize vendor_id
        @vendor_id = vendor_id
    end

    def self.exists? vendor_id
        self.redis.exists "vendor_id:{#vendor_id}:id"
    end

    def self.find vendor_id
        self.new vendor_id
    end

    def to_o
        User.from_redis(JSON.parse self.to_json)
    end

    def destroy
        self.name = nil
        self.age = nil
        self.income = nil
        self.province = nil
        self.categories.clear
        self.budget_categories.clear
        self.debt_checked.expire(0)
        self.goals_checked.expire(0)
        self.budget_checked.expire(0)
        self.budget_categories_checked.expire(0)
    end

    FIELDS = [:name, :age, :income, :province, :categories, :budget_categories]

    value :name
    value :age
    value :income
    value :province

    CATEGORIES = [:debt, :goals, :budget]

    set :categories

    counter :debt_checked
    counter :goals_checked
    counter :budget_checked

    BUDGET_CATEGORIES = ["Food", "Groceries", "Restaurants", "Electricity", "Cell phone", "Internet", "Cigarettes", "Entertainment"]

    BUDGET_CATEGORIES_MAP = Dish({
        "Food" => "Food expenditures",
        "Groceries" => "Food purchased from stores",
        "Restaurants" => "Food purchased from restaurants",
        "Electricity" => "Electricity for principal accommodation",
        "Cell phone" => "Cell phone, pager and handheld text messaging services",
        "Internet" => "Internet access services",
        "Cigarettes" => "Cigarettes",
        "Entertainment" => "Entertainment"
    })

    set :budget_categories

    counter :budget_categories_checked

    value :budget_categories_last

end

class Food < ActiveHash::Base
    fields :id, :ref_date, :geo, :food, :statistics, :vector, :coordinate, :value
end

class Household < ActiveHash::Base
    fields :id, :ref_date, :geo, :stat, :summary, :vector, :coordinate, :value
end

#hack in memory db
configure do
    # Redis
    Redis::Objects.redis = ConnectionPool.new(size: 5, timeout: 5) { Redis.new(:host => '127.0.0.1', :port => 6379) }

    # Provincial food
    # Load the CSV file and create stuff

    provincial_data_file = "02030002-eng.csv"
    _id = 1

    csv = CSV.new(File.open(provincial_data_file).read,:headers=>true, :header_converters => :symbol)

    csv.to_a.map {|row| Dish(row.to_hash) }.each do |row|
        Food.create(id:_id, ref_date: row.ref_date.to_i, geo: row.geo, food: row.food, statistics: row.statistics, vector: row.vector, coordinate: row.coordinate, value: row.value.to_f)
        _id += 1
    end

    Food.const_set('MAX_YEAR',Food.all.collect{|f|f.ref_date}.max)

    # Household info

    _id = 1

    household_data_file = "02030021-eng.csv"

    csv = CSV.new(File.open(household_data_file).read,:headers=>true, :header_converters => :symbol)

    csv.to_a.map {|row| Dish(row.to_hash) }.each do |row|
        Household.create(id:_id, ref_date: row.ref_date.to_i, geo: row.geo, stat: row.stat, summary: row.summary, vector: row.vector, coordinate: row.coordinate, value: row.value.to_f)
        _id += 1
    end

    Household.const_set('MAX_YEAR',Household.all.collect{|f|f.ref_date}.max)

end

delete '/user/:vendor_id' do
    user = RUser.find(params[:vendor_id])
    user.destroy
    status 204
end

# So many hacks
post '/user/:vendor_id' do
    user = RUser.find(params[:vendor_id])

    req_body = request.body.read

    # puts req_body

    if !req_body.nil? && !req_body.blank?
        body = JSON.parse(req_body)

        body.map do |k,v|
            r_obj = user.redis_objects
            if r_obj.include? k.to_sym
                case r_obj[k.to_sym][:type]
                when :value
                    user.send("#{k}=", v)
                when :set
                    user.send(k.to_sym).send(:clear)
                    if v && !v.empty?
                        v.each do |cat|
                            user.send(k.to_sym).send(:add, cat)
                        end
                    end
                end
            end
        end
    end

    User.find(user.id).to_json
end

QUESTIONS = Dish({
    debt: [
        "Did you remember to update your debt repayment progress this week?"
    ],
    goals: [
        "Did you remember to update your goal progress this week?",
        "Do you want to save up for something new?"
    ],
    budget: [
        "What did you do to save money today?",
        "Do you have any money saving tips to share?",
        "Did you remember to update your budget spending today?",
        "Do you want to add a new budget category to track?",
        "Did you spend more than $[average] on [category] last month?"
    ]
})

QUESTIONS_INDEX = QUESTIONS.send("_original_hash").collect{|k,v|v.collect{|g|g.gsub(/\[\w+\]\s/,"")}}.flatten

QUESTIONS_POSITIVE_ARRAY = [true, true, true, nil, nil, true, nil, false]
QUESTIONS_TYPE_ARRAY = ["yes/no", "yes/no", "yes/no", "any_text", "any_text", "yes/no", "yes/no", "yes/no"]

YES_ANSWERS = ["yeah!", "yes", "yeah" "yup", "of course", "of course!"]
NO_ANSWERS = ["no", "nope"]

POSITIVE_ADD = [nil, nil, nil, nil, nil, nil, nil, "Congrats! You are spending less than the average Canadian in your area!"]
NEGATIVE_ADD = [nil, nil, nil, nil, nil, nil, nil, "A good goal is to spend less than the average Canadian in your area :)"]

BUDGET_ADD_QUESTION = "How much do you spend on this each month, on average?"
BUDGET_ADD_QUESTION_ID = 1000

BUDGET_ADD_FINISHED = "Lowering spending even by a few percent in these categories is an easy way to save money.  I'll ask you some more questions later! ^_^"
BUDGET_ADD_FINISHED_ID = 1001

post '/answer/:vendor_id' do
    user = RUser.find(params[:vendor_id])

    answer_response = ""
    question_id = nil
    profanity = false
    store_text = nil
    b = nil
    q_positive = nil
    m_pos = nil

    req_body = request.body.read
    if !req_body.nil? && !req_body.blank?
        body = JSON.parse(req_body)

        puts "JSON answer: #{body}"

        if !body.nil?
            b = Dish(body)

	    # Hijack on 998
            # Hijack answers if question 999
            if b.question_id? && b.question_id.to_i == BUDGET_QUESTION_ID || b.question_id.to_i == NEW_BUDGET_QUESTION_ID
                # Gotta update budget categories
                # Answer will contain the category
                category = b.answer

                if RUser::BUDGET_CATEGORIES.include?(category)
                    # Add to user
                    user.budget_categories.add(category)
                    user.budget_categories_last = category
                    #fff... hack
                    return {question: BUDGET_ADD_QUESTION.gsub(/this/, category), question_id: BUDGET_ADD_QUESTION_ID, _type: "number"}.to_json
                end
            end

            #Hijack on question 1000
            if b.question_id? && b.question_id.to_i == BUDGET_ADD_QUESTION_ID
                # Answer will contain the number
                amount = b.answer
                last_category = user.budget_categories_last.value

                if last_category == nil
                    return {error: "invalid sequence"}.to_json
                end

                _avg = avg_household(RUser::BUDGET_CATEGORIES_MAP.send(last_category), user.province.nil? ? "Canada" : user.province.value)

                _avg = (_avg.value.to_i/12).round

                user.budget_categories_last = nil

                if b.answer.to_i < _avg.to_i
                    return {question: "Great!  The average person in your region spends $#{_avg.to_i} per month on #{last_category}.  You're already ahead of the game!  I will chat with you soon! ^_^", question_id: BUDGET_ADD_QUESTION_ID,_type: "none"}.to_json
                else
                    # on to next question...
                    return {question: "Cool!  The average person in your region spends $#{_avg.to_i} per month on #{last_category}, do you think you can do around $#{(_avg*0.9).to_i}?", question_id: BUDGET_ADD_FINISHED_ID, _type: "yes/no"}.to_json
                end

            end

            if b.question_id? && b.question_id.to_i == BUDGET_ADD_FINISHED_ID
                return {question: BUDGET_ADD_FINISHED, question_id: BUDGET_ADD_FINISHED_ID, _type: "none"}.to_json
            end

            user.budget_categories_last = nil

            if b.answer?
                if b.question_id?
                    question_id = b.question_id.to_i

                    if Obscenity.profane?(b.answer)
                        profanity = true
                    end

                    # ARG SO TIRED.... 
                    q_positive = QUESTIONS_POSITIVE_ARRAY.at(question_id)
                    if q_positive.nil?
                        answer_response = "Awesome!"
                        m_pos = true
                    elsif q_positive == true && YES_ANSWERS.include?(b.answer.downcase)
                        answer_response = "Great!"
                        m_pos = true
                    elsif q_positive == true && NO_ANSWERS.include?(b.answer.downcase)
                        answer_response = "Thats ok, we will help to keep you on track!"
                        m_pos = false
                    elsif q_positive == false && NO_ANSWERS.include?(b.answer.downcase)
                        answer_response = "Great!"
                        m_pos = true
                    else
                        answer_response = "Thats ok, we will help to keep you on track!"
                        m_pos = false
                    end
                end
            end
        end
    end

    if question_id && QUESTIONS_TYPE_ARRAY.at(question_id) == "any_text" && !profanity
        # Store it...
        ans_list = Redis::List.new("answers_list_#{question_id}")
        #first pick a random one

        if ans_list.size > 0
            rand = ans_list.to_a.sample
            answer_response = "#{answer_response}  Others have said - #{rand}"
        end

        #Add one
        ans_list << b.answer
    end

    if profanity
        answer_response = "No profanity please."
    end

    # Hijack question 6.....
    if question_id == 6 && m_pos
	return {question: NEW_BUDGET_QUESTION, question_id: NEW_BUDGET_QUESTION_ID, _type: "multiple_choice", choices: RUser::BUDGET_CATEGORIES.to_a - user.budget_categories.to_a}.to_json
    end

    {answer_response: modify_answer(answer_response, question_id, m_pos), question_id: question_id}.to_json
end

def modify_answer(answer_text, question_id, m_pos)
    ret_text = answer_text

    if m_pos && !POSITIVE_ADD.at(question_id).nil?
        ret_text = "#{answer_text} #{POSITIVE_ADD.at(question_id)}"
    end
    if !m_pos && !NEGATIVE_ADD.at(question_id).nil?
        ret_text = "#{answer_text} #{NEGATIVE_ADD.at(question_id)}"
    end

    ret_text
end

NEW_BUDGET_QUESTION = "Here are some other categories where you may be able to cut back.  Which of these do you want to talk about?"
NEW_BUDGET_QUESTION_ID = 998

BUDGET_QUESTION = "Here are some categories where you may be able to cut back.  Which of these do you want to talk about?"
BUDGET_QUESTION_ID = 999

get '/question/:vendor_id' do
    # find the user
    user = RUser.find(params[:vendor_id])

    # Find a question for the user based on prefs

    # Hijack questions if no budget categories
    has_no_budgets = user.budget_categories.size == 0
    if has_no_budgets
        return {question_id: BUDGET_QUESTION_ID, question: BUDGET_QUESTION, choices: RUser::BUDGET_CATEGORIES.to_a, _type: "multiple_choice"}.to_json
    end

    question = ""
    has_no_categories = true
    ret_val = nil
    bud_val = nil
    # TODO : proper expiry
    expiry_number = 5.minutes.to_i
    # [TimeDifference.between(Time.now,Chronic.parse("8am")).in_seconds,TimeDifference.between(Time.now,Chronic.parse("Tomorrow 8am")).in_seconds].min.to_i
    # Time.now.hour < 8, make "6am", otherwise "Tomorrow 6am"

    user.categories.each do |cat|

        puts "Processing category: #{cat}"

        has_no_categories = false

        val = user.send("#{cat}_checked").value

	puts "Val is: #{val}"

        question_size = QUESTIONS.send(cat).size

        budget_size = 0
        budget_categories_checked = 0

        if (cat == "budget")
            # keep track of budget size
            budget_size = user.budget_categories.size
            budget_categories_checked = user.budget_categories_checked.value

	    #Special case for question 6.... arg, which is question 3 in the budget section...
	    if val.to_i == 3
		# check to see if we have all categories
		rarr = RUser::BUDGET_CATEGORIES.to_a - user.budget_categories.to_a
		if rarr && rarr.blank?
                    puts "has all categories, attempt to skip this question"
		    # increment
	            user.budget_checked.incr
		    val = user.budget_checked.value
		end
	    end

        end

        if RUser::CATEGORIES.include?(cat.to_sym) && val < question_size
            # puts "Doing question for cat: #{cat}, val: #{val}"
            # do question for this category
            question = QUESTIONS.send(cat).at(val)

            puts "Preparing question: #{question}"

            # substitute if needed
            grp = question.scan(/\[(\w+)\]/)
            grp.flatten!

            puts "GRP: #{grp}, budget_size: #{budget_size}, budget_categories_checked: #{budget_categories_checked}"

            # do budget questions
            if grp && !grp.empty? && budget_size > 0 && budget_categories_checked < budget_size
                # we're on a question w/replacements

                puts "BUDGET-Preparing budget stuff with grp: #{grp}, and cats: #{user.budget_categories.to_a}"

                user_cat = user.budget_categories.to_a.at(budget_categories_checked)

                cat_question = question.gsub("[category]", user_cat)

                _avg = avg_household(RUser::BUDGET_CATEGORIES_MAP.send(user_cat), user.province.nil? ? "Canada" : user.province.value)

                puts "AVG: #{_avg}"

                cat_question = cat_question.gsub("[average]", (_avg.value/12.0).round.to_s)

                puts "Replacing question with #{cat_question}"
                question = cat_question

                user.budget_categories_checked.incr
                user.budget_categories_checked.expire(expiry_number)
                ret_val = val
                bud_val = budget_categories_checked

                break
            elsif
                budget_size > 0 && budget_categories_checked >= budget_size
                question = ""
            end

            ret_val = val
            user.send("#{cat}_checked").incr
            user.send("#{cat}_checked").expire(expiry_number)
            break

        end

    end

    if question.blank? && has_no_categories
        {question:"Which categories do you wish to subscribe to?", categories:RUser::CATEGORIES, budget_categories: RUser::BUDGET_CATEGORIES,_type:"initial"}.to_json
    elsif question.blank?
        {question:"Cool, we'll talk later! ^_^", _type: "none"}.to_json
    else
        q_index = QUESTIONS_INDEX.index(cleanup_question_for_id(question))
        {question:question, question_id: q_index, _type: QUESTIONS_TYPE_ARRAY.at(q_index)}.to_json
    end

end

get '/hi' do
    "Hello World!"
end

get '/test' do
    Food.all.to_json
end

get '/test2' do
    Food.find(4).to_json
end

private

def avg_household(category ,geo = "Canada")
    puts "Avg household for #{geo}, cateogory: #{category}"
    Household.where(ref_date: Household::MAX_YEAR, geo: geo, summary: category).first
end

def avg_food(category, geo = "Canada")
    puts "Avg food for #{geo}, category: #{category}"
    Food.where(ref_date: Food::MAX_YEAR, geo: geo, food: category).first
end

def cleanup_question_for_id(question)
    # question.gsub(/\d+\s/,"").gsub(/\$on\s\w+\s/,"$on ").gsub(/\$\[average\]\son/,"$on").gsub(/\[category\]\s/,"")
    question.gsub(/\d+\s/,"").gsub(/\$on\s\w+.*last?/,"$on last").gsub(/\$\[average\]\son/,"$on").gsub(/\[category\]\s/,"")
end
