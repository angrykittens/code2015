Sqrrrl is a personal financial assistant that will help you improve your financial health, and as a result of reduced stress, your well being as well. Using household spending and debt Data Sets as a benchmark, Sqrrrl helps you get out of debt, achieve your financial goals, budget your spending and on your way back to Healthy Living!  

The first step is having a quick chat with Sqrrrl's artificial intelligence bot to provide some information about yourself, so that the most relevant Data Sets can be used.  Next, you'll be able to add in the debts you want to pay off, see how your debt compares to other Canadians', and track your repayment progress. 

Once you're on track to pay off your debts, add some goals and track your progress towards them.  Sqrrrl will help you identify areas of spending where you're above or below the Canadian averages, so you can save as much as possible and achieve your financial goals more quickly!  Every day, you'll be prompted with a notification to answer a few questions about your progress in these areas, so you'll always be moving forward.

Sqrrrl is a whole new way of interacting with Open Data, making it accessible for everyone to help achieve their personal goals!


** NOTES: **

* Every conversation with Sqrrrl is a bit different. If you relaunch the app, Sqrrrl will prompt you with new questions! 
* ** For demonstration purposes, the daily turnover has been sped up to FIVE MINUTE intervals.  This will allow you to get multiple days' experience in a short period of time. **
* To simulate a new day, kill the app, wait five minutes, and launch again.  Every time you launch you'll get a different set of questions.
* All UUIDs have been added to the profile for the attached .ipa file. 
* Sqrrrl.ipa is in the root of our bitbucket.


** MINIMUM REQUIREMENTS: **

* Minimum iOS SDK 8.0
* Xcode 6.1.1
* Supported devices - iPhone (5, 5S, 6, 6 Plus)
* Supported orientation - Portrait only


** BUILD INSTRUCTIONS: **

* An .ipa file has been provided, but if you need to build, follow the directions below:
* Clone repo
* Select a valid developer provisioning profile in Xcode (Project Settings --> Build Settings --> Provisioning Profile in the Code Signing section)
* Build & Run