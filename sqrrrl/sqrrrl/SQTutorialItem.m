//
//  SQTutorialItem.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-22.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQTutorialItem.h"

@interface SQTutorialItem ()
@property (nonatomic) IBOutlet UIImageView *imageViewTutorial;
@property (nonatomic) IBOutlet UILabel *labelTitle;
@property (nonatomic) IBOutlet UILabel *labelBody;
@end

@implementation SQTutorialItem


- (void)populateWithImage:(UIImage*)image
                    title:(NSString*)title
                  andBody:(NSString*)body {
	
    self.imageViewTutorial.image = image;
    self.labelTitle.text = title;
    self.labelBody.text = body;
}

@end
