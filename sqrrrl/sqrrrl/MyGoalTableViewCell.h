//
//  GoalTableViewCell.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQGoal.h"

@interface MyGoalTableViewCell : UITableViewCell

@property (nonatomic) NSString* goalId;
@property (nonatomic) IBOutlet UILabel *labelGoalName;
@property (nonatomic) IBOutlet UILabel *labelGoalAmount;

- (void)populateWithGoal:(SQGoal*)goal;

@end
