
#import "NSDate+Extensions.h"

@implementation NSDate (Extensions)


+ (NSDateFormatter *)sharedFormatter
{
    static NSDateFormatter *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
                      sharedInstance = [[NSDateFormatter alloc] init];
                      sharedInstance.dateFormat = @"yyyy-MM-dd";
                  });
    return sharedInstance;
}

+ (NSDateFormatter *)sharedMonthFormatter
{
    static NSDateFormatter *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NSDateFormatter alloc] init];
        sharedInstance.dateFormat = @"yyyy-MM";
    });
    return sharedInstance;
}


+ (NSDate*)dateFromStringFormat:(NSString*)stringFormat
{
    NSDateFormatter* dateFormatter = [NSDate sharedFormatter];
    NSDate* date = [dateFormatter dateFromString:stringFormat];
    return date;
}

+ (NSDate*)noon
{
    NSUInteger units = NSCalendarUnitHour | NSCalendarUnitMinute;
    NSDateComponents *comps = [[NSCalendar currentCalendar] components:units fromDate:[NSDate date]];
    [comps setHour: 12];
    [comps setMinute: 0];
    NSDate *noon = [[NSCalendar currentCalendar] dateFromComponents:comps];
    return noon;
}

- (BOOL)isToday
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:self];
    NSDate *otherDate = [cal dateFromComponents:components];

    return [today isEqualToDate:otherDate];
}

- (NSDate*)midnight
{
    NSUInteger units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *comps = [[NSCalendar currentCalendar] components:units fromDate:self];
    NSDate *midnight = [[NSCalendar currentCalendar] dateFromComponents:comps];
    return midnight;
}

- (NSDate*)aWeekAgo
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:-7];
    NSDate *nextDate = [calendar dateByAddingComponents:offsetComponents toDate:self options:0];

    return nextDate;
}

- (NSDate*)previousDay
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:-1];
    NSDate *nextDate = [calendar dateByAddingComponents:offsetComponents toDate:self options:0];

    return nextDate;
}

- (NSDate*)nextDay
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:1];
    NSDate *nextDate = [calendar dateByAddingComponents:offsetComponents toDate:self options:0];

    return nextDate;
}

- (NSInteger)daysSinceDate:(NSDate*)dateSince
{
    NSDate *fromDate;
    NSDate *toDate;

    NSCalendar *calendar = [NSCalendar currentCalendar];

    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:dateSince];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:self];

    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];

    return difference.day;
}

@end
