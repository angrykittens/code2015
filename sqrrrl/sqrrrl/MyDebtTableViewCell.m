//
//  DebtTableViewCell.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "MyDebtTableViewCell.h"

@implementation MyDebtTableViewCell

- (void)awakeFromNib
{
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];
}


- (void)populateWithDebt:(SQDebt *)debt
{
    self.labelDebtName.text = debt.debtName;
    self.labelDebtAmount.text = [NSString stringWithFormat:@"$%0.2f", [debt currentAmount]];
}

@end
