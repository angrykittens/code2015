
#import <UIKit/UIKit.h>

@interface NSDate (Extensions)

+ (NSDate*)dateFromStringFormat:(NSString*)stringFormat;
+ (NSDate*)noon;

- (BOOL)isToday;
- (NSDate*)midnight;
- (NSDate*)aWeekAgo;
- (NSDate*)previousDay;
- (NSDate*)nextDay;
- (NSInteger)daysSinceDate:(NSDate*)dateSince;

@end
