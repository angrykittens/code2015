//
//  UIView+SQLayoutConstraints.h
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SQLayoutConstraints)

- (void)constrainAttribute:(NSLayoutAttribute)attribute toConstant:(CGFloat)constant;
- (void)constrainChildView:(UIView *)view withSameAttribute:(NSLayoutAttribute)attribute;
- (void)constrainChildView:(UIView *)view withSameAttribute:(NSLayoutAttribute)attribute withConstant:(CGFloat)constant;
- (void)constrainChildView:(UIView *)view withSameAttributes:(NSLayoutAttribute)attribute1, ...NS_REQUIRES_NIL_TERMINATION;

- (void)constrainViewsVertically:(NSArray *)views withPaddings:(NSArray *)paddings;
- (void)constrainViewsVerticallyWithinParent:(NSArray *)views withPaddings:(NSArray *)paddings;
- (void)constrainViewsVerticallyWithinParent:(NSArray *)views withPaddings:(NSArray *)paddings withTopPadding:(CGFloat)topPadding andBottomPadding:(CGFloat)bottomPadding;

+ (NSLayoutConstraint *)contraintForView:(UIView *)view1 toView:(UIView *)view2 withSameAttribute:(NSLayoutAttribute)attribute;
+ (NSArray *)constraintsForView:(UIView *)view1 toView:(UIView *)view2 withSameAttributes:(NSLayoutAttribute)attribute1, ...NS_REQUIRES_NIL_TERMINATION;
+ (void)disableTranslateAutoresizingMaskIntoContraintsForViews:(UIView *)view1, ...NS_REQUIRES_NIL_TERMINATION;

@end
