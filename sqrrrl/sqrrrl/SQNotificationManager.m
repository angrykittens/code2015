
#import "SQNotificationManager.h"
#import "SQDebug.h"
#import "NSDate+Extensions.h"


@implementation SQNotificationManager

+ (SQNotificationManager *)sharedInstance
{
    static SQNotificationManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
                      sharedInstance = [[SQNotificationManager alloc] init];
                  });
    return sharedInstance;
}

- (void)didReceiveLocalNotification:(UILocalNotification *)notification
{
    SQLog(@"Received Local Notification %@", notification.alertBody);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"daily" object:nil];
}

- (void)scheduleDailyReminders
{
    [self registerForLocalNotifications];
    [self scheduleLocalNotificationWithTitle:@"Hey! How's it going? Wanna talk to the Sqrrl!" sound:NO];
}

- (void)registerForLocalNotifications
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    // The following line must only run under iOS 8. This runtime check prevents
    // it from running if it doesn't exist (such as running under iOS 7 or earlier).
    UIUserNotificationSettings *userNotificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings: userNotificationSettings];
#endif
}

- (void)scheduleLocalNotificationWithTitle:(NSString *)body sound:(BOOL)sound
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [self date_8pm_tomorrow];
    localNotification.alertBody = body;
    localNotification.alertAction = @"";
    localNotification.hasAction = YES;
    localNotification.repeatInterval = NSCalendarUnitDay;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (NSDate *)date_8pm_tomorrow
{
    return [self dateWithHour:20 minute:0 second:0];
}

- (NSDate *)dateWithHour:(NSInteger)hour
                  minute:(NSInteger)minute
                  second:(NSInteger)second
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components: NSCalendarUnitYear|
                                    NSCalendarUnitMonth|
                                    NSCalendarUnitDay
                                               fromDate:[[NSDate date] nextDay]];
    [components setHour:hour];
    [components setMinute:minute];
    [components setSecond:second];
    NSDate *newDate = [calendar dateFromComponents:components];
    return newDate;
}

@end
