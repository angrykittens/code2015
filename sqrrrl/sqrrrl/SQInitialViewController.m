//
//  SQInitialViewController.m
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQInitialViewController.h"
#import "SQNetworkManager.h"
#import "UserManager.h"
#import "SQTutorialViewController.h"

@interface SQInitialViewController ()

@property (nonatomic, strong) SQNetworkManager *getUserHelper;

@end

@implementation SQInitialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tutorial"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    
//    SQTutorialViewController *tur = [[SQTutorialViewController alloc] init];
//    [tur setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//    [self presentViewController:tur animated:YES completion:nil];
//    return;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"tutorial"]) {

        //do network call using the vendorId
        SQNetworkManager *getUser = [[SQNetworkManager alloc] init];
        self.getUserHelper = getUser;
        [getUser makePostRequestWithEndpoint:[@"http://sqrrrl.thenoos.net/user/" stringByAppendingString :[UIDevice currentDevice].identifierForVendor.UUIDString] json:@{} responseBlock:^(NSDictionary *json, NSError *error) {
            
            if (json) {
                [[UserManager sharedInstance] createUserWithJson:json];
            }
            
            [self performSegueWithIdentifier:@"showMainUI" sender:self];
            
        }];

    } else {

        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tutorial"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        SQTutorialViewController *tur = [[SQTutorialViewController alloc] init];
        [tur setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:tur animated:YES completion:nil];
        
    }

}

@end
