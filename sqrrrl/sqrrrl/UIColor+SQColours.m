//
//  UIColor+SQColours.m
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "UIColor+SQColours.h"

@implementation UIColor (SQColours)

+ (UIColor *)sqDarkBlueColour
{
    return [UIColor colorWithRed:11.0f/255.0f green:72.0f/255.0f blue:107.0f/255.0f alpha:1.0f];
}

+ (UIColor *)sqPinkColour
{
    return [UIColor colorWithRed:245.0f/255.0f green:67.0f/255.0f blue:113.0f/255.0f alpha:1.0f];
}

+ (UIColor *)sqDebtRedColour
{
    return [UIColor colorWithRed:0.961 green:0.263 blue:0.443 alpha:1.000];
}

+ (UIColor *)sqYellowColour
{
    return [UIColor colorWithRed:0.988 green:0.851 blue:0.086 alpha:1.000];
}

+ (UIColor *)sqDarkSeaFormColour
{
    return [UIColor colorWithRed:0.196 green:0.348 blue:0.255 alpha:1.000];
}

+ (UIColor *)sqSeaFoamColour
{
    return [UIColor colorWithRed:121.0f/255.0f green:189.0f/255.0f blue:154.0f/255.0f alpha:1.0f];
}

@end
