//
//  SQKeyboardAccessoryView.h
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol SQKeyboardAccessoryViewDelegate
- (void)didSelectDone;

@optional
- (BOOL)canSelectPrevious;
- (BOOL)canSelectNext;
- (void)didSelectPrevious;
- (void)didSelectNext;
@end

@interface SQKeyboardAccessoryView : UIView

@property (nonatomic) id<SQKeyboardAccessoryViewDelegate> delegateKeyboardAccessory;

+ (CGFloat)heightOfAccessoryView;
- (void)updateAccessoryView;
- (void)hidePreviousNextButtons;

@end
