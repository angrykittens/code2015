//
//  DebtTableViewCell.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQDebt.h"

@interface MyDebtTableViewCell : UITableViewCell

@property (nonatomic) NSString* debtId;
@property (nonatomic) IBOutlet UILabel *labelDebtName;
@property (nonatomic) IBOutlet UILabel *labelDebtAmount;

- (void)populateWithDebt:(SQDebt*)debt;

@end
