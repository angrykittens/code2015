//
//  UIFont+.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "UIFont+SQFont.h"

@implementation UIFont (SQ)


+ (UIFont *)sqFontLightOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:size];
}


+ (UIFont *)sqFontRegularOfSize:(CGFloat)size
{
    return [UIFont fontWithName:@"HelveticaNeue" size:size];
}

@end
