#import "SQDebug.h"

#define MAX_COLUMN_WIDTH  (75)  // Limits the maximum number of characters used in the console display for function name


BOOL flareHasFired;
BOOL disableLogging;


@implementation SQDebug

+ (void)log:(NSString *)output
{
    NSLog(@"%@", output);
}

+ (void)disableLogging:(BOOL)loggingDisabled
{
    disableLogging = loggingDisabled;
}

+ (void)   log:(char *)fileName
    lineNumber:(NSInteger)lineNumber
      function:(const char *)stringFunction
         input:(NSString *)input, ...
{
    if (disableLogging == YES) return;

    va_list argList;
    NSString *filePath, *formatStr;

    filePath = [[NSString alloc] initWithBytes:fileName length:strlen(fileName) encoding:NSUTF8StringEncoding];

    va_start(argList, input);
    formatStr = [[NSString alloc] initWithFormat:input arguments:argList];
    va_end(argList);

    NSString *stringLine = [NSString stringWithFormat:@"<%3.0ld>: ", (long)lineNumber];

    NSMutableString *string_out = [NSMutableString stringWithString:[NSString stringWithFormat:@"%s", stringFunction]];
    while (string_out.length > MAX_COLUMN_WIDTH - stringLine.length) [string_out deleteCharactersInRange:NSMakeRange(string_out.length - 1, 1)];

    [string_out appendString:stringLine];
    [string_out appendString:formatStr];

    [SQDebug log:string_out];
}

+ (void) error:(char *)fileName
    lineNumber:(NSInteger)lineNumber
      function:(const char *)stringFunction
         input:(NSString *)input, ...
{
    if (disableLogging == YES) return;

    va_list argList;
    NSString *filePath, *formatStr;

    filePath = [[NSString alloc] initWithBytes:fileName length:strlen(fileName) encoding:NSUTF8StringEncoding];

    va_start(argList, input);
    formatStr = [[NSString alloc] initWithFormat:input arguments:argList];
    va_end(argList);

    NSLog(@"\n\n ERROR\n  File: %s\n  Line: %ld\n  File: %s\n Cause: %@ \n\n", [[filePath lastPathComponent] UTF8String], (long)lineNumber, stringFunction, formatStr);

    SQBreakPoint;

    exit(1);
}


+ (void)assert:(NSInteger)evaluate
        output:(char *)fileName
    lineNumber:(NSInteger)lineNumber
      function:(const char *)stringFunction
         input:(NSString *)input, ...
{
    if (evaluate == YES) return;

    va_list argList;
    NSString *filePath, *formatStr;

    filePath = [[NSString alloc] initWithBytes:fileName length:strlen(fileName) encoding:NSUTF8StringEncoding];

    va_start(argList, input);
    formatStr = [[NSString alloc] initWithFormat:input arguments:argList];
    va_end(argList);

    NSString *stringOut = [NSString stringWithFormat:@"\n\nASSERTION ERROR\n File: %s\n  Line: %ld \n%s \n  Cause: %@ \n\n",
                           [[filePath lastPathComponent] UTF8String],
                           (long)lineNumber,
                           stringFunction,
                           formatStr];

    [SQDebug log:stringOut];

    SQBreakPoint;

    exit(1);
}

+ (void)assertNotNil:(NSObject*)evaluate
              output:(char *)fileName
          lineNumber:(int)lineNumber
            function:(const char *)stringFunction
{
    if (evaluate != nil) {
        return;
    }
    
    NSString *filePath;
    
    filePath = [[NSString alloc] initWithBytes:fileName length:strlen(fileName) encoding:NSUTF8StringEncoding];
    
    NSString *formatStr = @"Cannot be nil";
    
    NSString *stringOut = [NSString stringWithFormat:@"\n\nASSERTION ERROR\n File: %s\n  Line: %d \n%s \n  Cause: %@ \n\n",
                           [[filePath lastPathComponent] UTF8String],
                           lineNumber,
                           stringFunction,
                           formatStr];
    
    [SQDebug log:stringOut];
    
    SQBreakPoint;
    
    exit(1);
}

+ (void)assert:(id)object
        output:(char *)fileName
    lineNumber:(NSInteger)lineNumber
      function:(const char *)stringFunction
{
    if (object != nil) return;

    NSString *filePath, *formatStr;

    filePath = [[NSString alloc] initWithBytes:fileName length:strlen(fileName) encoding:NSUTF8StringEncoding];

    formatStr = @"object cannot be nil";

    NSString *stringOut = [NSString stringWithFormat:@"\n\nASSERTION ERROR\n File: %s\n  Line: %ld \n%s \n  Cause: %@ \n\n",
                           [[filePath lastPathComponent] UTF8String],
                           (long)lineNumber,
                           stringFunction,
                           formatStr];

    [SQDebug log:stringOut];

    SQBreakPoint;

    exit(1);
}

+ (void)pointer:(void *)pointer
         output:(char *)fileName
     lineNumber:(NSInteger)lineNumber
       function:(const char *)stringFunction
{
    if (pointer != nil) return;

    NSLog(@"SQ_Assert Null Pointer");
    NSLog(@"Filename: %s", fileName);
    NSLog(@"Line: %ld", (long)lineNumber);
    NSString *filePath = [[NSString alloc] initWithBytes:fileName length:strlen(fileName) encoding:NSUTF8StringEncoding];
    NSLog(@"filePath: %@", [filePath lastPathComponent]);

    exit(1);
}

+ (void)trace:(const char *)stringFunction
{
    if (disableLogging == YES) return;

    NSString *string_out = [NSString stringWithFormat:@"%s", stringFunction];
    [SQDebug log:string_out];
}

+ (void)flare
{
    if (flareHasFired == YES) return;

    flareHasFired = YES;
    [SQDebug log:@" "];
    [SQDebug log:@">>>>>>>>>>>>>>>>>>>>>> Flare"];
    [SQDebug log:@" "];
    [NSThread sleepForTimeInterval:1.5];
}


@end




