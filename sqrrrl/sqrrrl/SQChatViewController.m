//
//  SQChatViewController.m
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQChatViewController.h"
#import "SqrrrlChatCollectionViewCell.h"
#import "UserChatCollectionViewCell.h"
#import "SqrrrlChat.h"
#import "UserChat.h"
#import "SQInputButton.h"
#import "UIView+SQLayoutConstraints.h"
#import "UIColor+SQColours.h"
#import "UserManager.h"
#import "SQNetworkManager.h"
#import "SQDebtManager.h"
#import "SQDebt.h"

#import "SQGoalManager.h"
#import "SQGoal.h"

@interface SQChatViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewBottomConstraint;

@property (strong, nonatomic) NSMutableArray * chatBubbles;

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;


@property (nonatomic, strong) NSArray * mcButtonsArray;

@property (nonatomic, strong) SQNetworkManager * updateUserSession;
@property (nonatomic, strong) SQNetworkManager * nextQuestionSession;


@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, strong) SQDebt * debtToSave;
@property BOOL userHasNoDebts;

@property (nonatomic, strong) SQGoal * goalToSave;
@property BOOL userHasNoGoals;

@end

@implementation SQChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSArray * images = @[@"motivation1",@"motivation2",@"motivation3",@"motivation4"];
    
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.backgroundImageView.image = [UIImage imageNamed:images[arc4random() %(4)]];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.textField resignFirstResponder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (!self.chatBubbles.count) {
        self.chatBubbles = [NSMutableArray array];

        //fake
        SqrrrlChat * initalChatBubble = [[SqrrrlChat alloc] initWithText:@"Hey there!"];
        initalChatBubble.responseType = ChatResponseTypeMultipleChoice;
        initalChatBubble.mcResponseOptions = @[@"Hi!"];
        [self.chatBubbles addObject:initalChatBubble];
        [self.collectionView reloadData];
        [self setupInputViewWithChat:initalChatBubble];
        
    }
    
}

-(void)setupInputViewWithChat:(BaseChat*) chat {
    
    self.textField.text = @"";
    for (UIButton * button in self.mcButtonsArray) {
        [button removeFromSuperview];
    }
    
    switch (chat.responseType) {
        case ChatResponseTypeMultipleChoice: {
            
            self.mcButtonsArray = [NSArray array];
            CGFloat buttonHeight = chat.mcResponseOptions.count > 5 ? 25 : 40;
            CGFloat buttonPadding = chat.mcResponseOptions.count > 5 ? 4 : 7;
            
            for (NSString * potentialAnswer in chat.mcResponseOptions) {
                
                SQInputButton * button = [[SQInputButton alloc] init];
                [button representResponse:potentialAnswer];
                [button addTarget:self action:@selector(inputButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [button setBackgroundColor:[UIColor sqDarkBlueColour]];
                button.translatesAutoresizingMaskIntoConstraints = NO;
                [button constrainAttribute:NSLayoutAttributeHeight toConstant:buttonHeight];
                button.layer.cornerRadius = 8;
                [self.inputView addSubview:button];
                [self.inputView constrainChildView:button withSameAttribute:NSLayoutAttributeCenterX];
                self.mcButtonsArray = [self.mcButtonsArray arrayByAddingObject:button];
                
            }
            
            if (self.mcButtonsArray.count) {
                [self.inputView constrainViewsVerticallyWithinParent:self.mcButtonsArray withPaddings:@[[NSNumber numberWithFloat:buttonPadding]] withTopPadding:buttonPadding andBottomPadding:7];
                [self.inputView layoutIfNeeded];
            }

            
            
            CGFloat height = chat.mcResponseOptions.count * (buttonHeight + buttonPadding) + 7;

            self.inputViewHeightConstraint.constant = height;
            
            self.textField.hidden = YES;
            self.sendButton.hidden = YES;
            

            [UIView animateWithDuration:0.3f animations:^{
                [self.view layoutIfNeeded];
                [self.collectionView setContentInset:UIEdgeInsetsMake(self.collectionView.contentInset.top, self.collectionView.contentInset.left, height + 49+20, self.collectionView.contentInset.right)];
            }];
            
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.chatBubbles.count -1 inSection:0 ] atScrollPosition:UICollectionViewScrollPositionBottom | UICollectionViewScrollPositionLeft animated:YES];
        } break;
            
        case ChatResponseTypeNumber: {
            
            CGFloat height = 50;
            self.inputViewHeightConstraint.constant = height;

            self.textField.hidden = NO;
            self.sendButton.hidden = NO;
            
            self.textField.keyboardType = UIKeyboardTypeNumberPad;
            
            [UIView animateWithDuration:0.3f animations:^{
                [self.view layoutIfNeeded];
                [self.collectionView setContentInset:UIEdgeInsetsMake(self.collectionView.contentInset.top, self.collectionView.contentInset.left, height + 49+20, self.collectionView.contentInset.right)];
                [self.inputView layoutIfNeeded];
            }];
            
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.chatBubbles.count -1 inSection:0 ] atScrollPosition:UICollectionViewScrollPositionBottom | UICollectionViewScrollPositionLeft animated:YES];
            
            [self.textField becomeFirstResponder];
            
        } break;
            
        case ChatResponseTypeText: {
            CGFloat height = 50;
            self.inputViewHeightConstraint.constant = height;
            
            self.textField.hidden = NO;
            self.sendButton.hidden = NO;
            
            self.textField.keyboardType = UIKeyboardTypeDefault;
            
            [UIView animateWithDuration:0.3f animations:^{
                [self.view layoutIfNeeded];
                [self.collectionView setContentInset:UIEdgeInsetsMake(self.collectionView.contentInset.top, self.collectionView.contentInset.left, height + 49+20, self.collectionView.contentInset.right)];
            }];
            
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.chatBubbles.count -1 inSection:0 ] atScrollPosition:UICollectionViewScrollPositionBottom | UICollectionViewScrollPositionLeft animated:YES];
            
            [self.textField becomeFirstResponder];
        } break;
            
        default:
            break;
    }

}

- (IBAction)sendButtonPressed:(id)sender {
    
    [self submitInput:self.textField.text];
    [self.textField resignFirstResponder];
    
    self.inputViewHeightConstraint.constant = 0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        [self.collectionView setContentInset:UIEdgeInsetsMake(self.collectionView.contentInset.top, self.collectionView.contentInset.left, 49+20, self.collectionView.contentInset.right)];
    }];
    
    
}

-(IBAction)inputButtonPressed:(SQInputButton*)sender {
    [self submitInput:sender.correspondingResponse];
    
    self.inputViewHeightConstraint.constant = 0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        [self.collectionView setContentInset:UIEdgeInsetsMake(self.collectionView.contentInset.top, self.collectionView.contentInset.left, 49+20, self.collectionView.contentInset.right)];
    }];
}


-(void)addNewChatBubble:(BaseChat*)chatBubble {
    [self.chatBubbles addObject:chatBubble];
    
    [self.collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:self.chatBubbles.count-1 inSection:0]]];
    
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.chatBubbles.count -1 inSection:0 ] atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
}

-(void)getNewChatBubbleFromServer {
    
    
    SqrrrlChat * nextChatBubble;
    //local chat bubbles
    if ([UserManager sharedInstance].hasEmptyFields) {
        if (![UserManager sharedInstance].name) {
            nextChatBubble = [[SqrrrlChat alloc] initWithText:@"I'm Sqrrrl. What's your name?"];
            nextChatBubble.correspondingObject = @"user.name";
            nextChatBubble.responseType = ChatResponseTypeText;
            
        } else if (![UserManager sharedInstance].age) {
            
            SqrrrlChat * intermediateChatBubble = [[SqrrrlChat alloc] initWithText:@"I’ll be your personal financial assistant, working with you to improve your financial health."];
            [self addNewChatBubble:intermediateChatBubble];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                SqrrrlChat * ageChatBubble = [[SqrrrlChat alloc] initWithText:[NSString stringWithFormat:@"How old are you, %@?", [UserManager sharedInstance].name]];
                ageChatBubble.correspondingObject = @"user.age";
                ageChatBubble.responseType = ChatResponseTypeNumber;
                [self addNewChatBubble:ageChatBubble];
                [self setupInputViewWithChat:ageChatBubble];
            });
            
        } else if (![UserManager sharedInstance].income) {
            nextChatBubble = [[SqrrrlChat alloc] initWithText:@"What’s your approximate household income annually? Don't worry, I'll keep it safe."];
            nextChatBubble.correspondingObject = @"user.income";
            nextChatBubble.responseType = ChatResponseTypeNumber;
        } else if (![UserManager sharedInstance].province) {
            nextChatBubble = [[SqrrrlChat alloc] initWithText:@"Great. And what province do you live in?"];
            nextChatBubble.correspondingObject = @"user.province";
            nextChatBubble.responseType = ChatResponseTypeMultipleChoice;
            nextChatBubble.mcResponseOptions = @[ @"Newfoundland and Labrador", @"Prince Edward Island", @"Nova Scotia", @"New Brunswick", @"Quebec", @"Ontario", @"Manitoba", @"Saskatchewan", @"Alberta", @"British Columbia", @"Yukon", @"Northwest Territories", @"Nunavut"];
            
            
        } else {
            //send it
            
            self.updateUserSession = [[SQNetworkManager alloc] init];
            [self.updateUserSession makePostRequestWithEndpoint:[@"http://sqrrrl.thenoos.net/user/" stringByAppendingString:[UIDevice currentDevice].identifierForVendor.UUIDString]
                                                           json:@{@"name":[UserManager sharedInstance].name,
                                                                  @"age":[NSString stringWithFormat:@"%li",(long)[UserManager sharedInstance].age.integerValue],
                                                                  @"income":[NSString stringWithFormat:@"%li",(long)[UserManager sharedInstance].income.integerValue],
                                                                  @"province":[UserManager sharedInstance].province,
                                                                  @"categories":@[@"debt", @"goals", @"budget"]
                                                                  }
                                                  responseBlock:^(NSDictionary *json, NSError *error) {
                                                      [UserManager sharedInstance].hasEmptyFields = NO;
                                                      [self getNewChatBubbleFromServer];
                                                      
                                                  }];
        }
        
    } else if (![SQDebtManager debtManager].debts.count && !self.userHasNoDebts) {
        
        //see if they have any debts
        if (!self.debtToSave) {
            self.debtToSave = [SQDebt debt];
            SqrrrlChat * debtPromptChat = [[SqrrrlChat alloc] initWithText:@"Okay, let's talk about debts. Do you have any debts?"];
            debtPromptChat.responseType = ChatResponseTypeMultipleChoice;
            debtPromptChat.mcResponseOptions = @[@"No",@"Credit Card",@"Student Loan",@"Loan",@"Line of Credit",@"Other"];
            debtPromptChat.correspondingObject = @"debt.debtName";
            
            [self addNewChatBubble:debtPromptChat];
            [self setupInputViewWithChat:debtPromptChat];
        } else if (self.debtToSave.debtName && [self.debtToSave currentAmount] == 0) {
            SqrrrlChat * debtPromptChat = [[SqrrrlChat alloc] initWithText:@"What's the balance on this debt?"];
            debtPromptChat.responseType = ChatResponseTypeNumber;
            debtPromptChat.correspondingObject = @"debt.debtAmount";
            
            [self addNewChatBubble:debtPromptChat];
            [self setupInputViewWithChat:debtPromptChat];
        } else if ([self.debtToSave currentAmount] != 0) {
            [[SQDebtManager debtManager] addDebt:self.debtToSave];
            
            SqrrrlChat * debtPromptChat = [[SqrrrlChat alloc] initWithText:@"Great, now I can help you pay it off! You can also add the rest later in the My Debts tab."];
            
            [self addNewChatBubble:debtPromptChat];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self getNewChatBubbleFromServer];
            });
        }

    } else if (![SQGoalManager goalManager].goals.count && !self.userHasNoGoals) {
        
        //see if they have any goals
        if (!self.goalToSave) {
            self.goalToSave = [SQGoal goal];
            [self.goalToSave setGoalAmount:-1];
            SqrrrlChat * goalPromptChat = [[SqrrrlChat alloc] initWithText:@"Now let’s talk about your goals! \nWhat are you saving up for right now?"];
            goalPromptChat.responseType = ChatResponseTypeMultipleChoice;
            goalPromptChat.mcResponseOptions = @[@"Nothing",@"Retirement",@"Rainy Day",@"Vacation",@"Other"];
            goalPromptChat.correspondingObject = @"goal.goalName";
            
            [self addNewChatBubble:goalPromptChat];
            [self setupInputViewWithChat:goalPromptChat];
        } else if (self.goalToSave.goalName && [self.goalToSave goalTarget] == 0) {
            SqrrrlChat * goalPromptChat = [[SqrrrlChat alloc] initWithText:@"What’s your target amount?"];
            goalPromptChat.responseType = ChatResponseTypeNumber;
            goalPromptChat.correspondingObject = @"goal.goalTarget";
            
            [self addNewChatBubble:goalPromptChat];
            [self setupInputViewWithChat:goalPromptChat];
        } else if (self.goalToSave.currentAmount == -1) {
            SqrrrlChat * goalPromptChat = [[SqrrrlChat alloc] initWithText:@"And how much have you saved so far?"];
            goalPromptChat.responseType = ChatResponseTypeNumber;
            goalPromptChat.correspondingObject = @"goal.goalAmount";
            
            [self addNewChatBubble:goalPromptChat];
            [self setupInputViewWithChat:goalPromptChat];
        } else if ([self.goalToSave currentAmount] != -1) {
            [[SQGoalManager goalManager] addGoal:self.goalToSave];
            
            SqrrrlChat * goalPromptChat = [[SqrrrlChat alloc] initWithText:@"Amazing!  You can always edit or add more goals in the My Goals tab.\nNow that you’ve set some goals, let’s help you get there by setting up some budgets.\nIf you can spend a bit less in some areas, you’ll be able to put that extra cash towards your goals!"];
            
            [self addNewChatBubble:goalPromptChat];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self getNewChatBubbleFromServer];
            });
        }
        
    } else {
        
        //get from the server
        self.nextQuestionSession = [[SQNetworkManager alloc] init];
        [self.nextQuestionSession makeGetRequestWithEndpoint:[@"http://sqrrrl.thenoos.net/question/" stringByAppendingString:[UIDevice currentDevice].identifierForVendor.UUIDString] responseBlock:^(NSDictionary *json, NSError *error) {
            
            if ([json objectForKey:@"categories"]) {
                //initial setup of categories
                
                SqrrrlChat * sqrrlQuestion = [[SqrrrlChat alloc] initWithText:[json objectForKey:@"question"]];
                sqrrlQuestion.responseType = ChatResponseTypeMultipleChoice;
                sqrrlQuestion.correspondingObject = @"user.categories";
                sqrrlQuestion.mcResponseOptions = [json objectForKey:@"categories"];
                sqrrlQuestion.questionId = [json objectForKey:@"question_id"];
                
                [self addNewChatBubble:sqrrlQuestion];
                [self setupInputViewWithChat:sqrrlQuestion];
                
            } else {
                SqrrrlChat * sqrrlQuestion = [[SqrrrlChat alloc] initWithText:[json objectForKey:@"question"]];
                
                if ([[json objectForKey:@"_type"] isEqualToString:@"yes/no"]) {
                    sqrrlQuestion.responseType = ChatResponseTypeMultipleChoice;
                    sqrrlQuestion.mcResponseOptions = @[@"Yes", @"Nope"];
                } else if ([[json objectForKey:@"_type"] isEqualToString:@"multiple_choice"]) {
                    sqrrlQuestion.responseType = ChatResponseTypeMultipleChoice;
                    sqrrlQuestion.mcResponseOptions = [json objectForKey:@"choices"];
                } else if ([[json objectForKey:@"_type"] isEqualToString:@"none"]) {
                    sqrrlQuestion.responseType = ChatResponseTypeLastQuestion;
                } else if ([[json objectForKey:@"_type"] isEqualToString:@"any_text"]) {
                    sqrrlQuestion.responseType = ChatResponseTypeText;
                } else if ([[json objectForKey:@"_type"] isEqualToString:@"number"]) {
                    sqrrlQuestion.responseType = ChatResponseTypeNumber;
                }
                
                sqrrlQuestion.questionId = [json objectForKey:@"question_id"];
                
                [self addNewChatBubble:sqrrlQuestion];
                [self setupInputViewWithChat:sqrrlQuestion];
            }
            
            
        }];
        
    }
    
    if (nextChatBubble) {
        [self addNewChatBubble:nextChatBubble];
        [self setupInputViewWithChat:nextChatBubble];
        
    }

    
}


-(void)submitInput:(NSString *)input {
    
    SqrrrlChat * previousQuestion = [self.chatBubbles lastObject];
    

    
    UserChat * userResponse = [[UserChat alloc] initWithText:input];
    [self.chatBubbles addObject:userResponse];
    
    [self.collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:self.chatBubbles.count-1 inSection:0]]];
    
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.chatBubbles.count -1 inSection:0 ] atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
    
    NSArray * correspondingObjectArray = [previousQuestion.correspondingObject componentsSeparatedByString:@"."];
    
    if ([[correspondingObjectArray firstObject] isEqualToString:@"user"]) {
        if ([[UserManager sharedInstance] respondsToSelector:NSSelectorFromString([correspondingObjectArray lastObject])]) {
            [[UserManager sharedInstance] setValue:input forKeyPath:[correspondingObjectArray lastObject]];
        }
        
        if ([previousQuestion.correspondingObject isEqualToString:@"user.categories"]) {
            // do something special
            
            self.updateUserSession = [[SQNetworkManager alloc] init];
            
            [UserManager sharedInstance].categories = @[input];
            
            [self.updateUserSession makePostRequestWithEndpoint:[@"http://sqrrrl.thenoos.net/user/" stringByAppendingString:[UIDevice currentDevice].identifierForVendor.UUIDString] json:@{@"categories": @[input]} responseBlock:^(NSDictionary *json, NSError *error) {
                

                [self getNewChatBubbleFromServer];
                
            }];
            
            return;
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self getNewChatBubbleFromServer];
            });

        }

    } else if ([[correspondingObjectArray firstObject] isEqualToString:@"debt"]) {
        
        if ([input isEqualToString:@"No"]) {
            self.userHasNoDebts = YES;
            
            SqrrrlChat * response = [[SqrrrlChat alloc] initWithText:@"Great! You can always add some later in the My Debts tab."];
            [self addNewChatBubble:response];
            
        } else if ([self.debtToSave respondsToSelector:NSSelectorFromString([correspondingObjectArray lastObject])]) {
            [self.debtToSave setValue:input forKeyPath:[correspondingObjectArray lastObject]];
        } else if ([[correspondingObjectArray lastObject] isEqualToString:@"debtAmount"]) {
            [self.debtToSave setDebtAmount:[input floatValue]];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getNewChatBubbleFromServer];
        });

        
    } else if ([[correspondingObjectArray firstObject] isEqualToString:@"goal"]) {
        
        if ([input isEqualToString:@"Nothing"]) {
            self.userHasNoGoals = YES;
            
            SqrrrlChat * response = [[SqrrrlChat alloc] initWithText:@"That's fine. Check out the My Goals tab when you want to set one up."];
            [self addNewChatBubble:response];
            
        } else if ([self.goalToSave respondsToSelector:NSSelectorFromString([correspondingObjectArray lastObject])]) {
            [self.goalToSave setValue:input forKeyPath:[correspondingObjectArray lastObject]];
        } else if ([[correspondingObjectArray lastObject] isEqualToString:@"goalAmount"]) {
            [self.goalToSave setGoalAmount:[input floatValue]];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getNewChatBubbleFromServer];
        });
        
        
    } else if (previousQuestion.questionId){
        
        self.nextQuestionSession = [[SQNetworkManager alloc] init];
        [self.nextQuestionSession makePostRequestWithEndpoint:[@"http://sqrrrl.thenoos.net/answer/" stringByAppendingString:[UIDevice currentDevice].identifierForVendor.UUIDString]
                                                         json:@{
                                                                @"question_id":previousQuestion.questionId,
                                                                @"answer":input
                                                                }
                                                responseBlock:^(NSDictionary *json, NSError *error) {
                                                    
                                                    if ([json objectForKey:@"question"]) {
                                                        
                                                        SqrrrlChat * sqrrlQuestion = [[SqrrrlChat alloc] initWithText:[json objectForKey:@"question"]];
                                                        
                                                        if ([[json objectForKey:@"_type"] isEqualToString:@"yes/no"]) {
                                                            sqrrlQuestion.responseType = ChatResponseTypeMultipleChoice;
                                                            sqrrlQuestion.mcResponseOptions = @[@"Yes", @"Nope"];
                                                        } else if ([[json objectForKey:@"_type"] isEqualToString:@"multiple_choice"]) {
                                                            sqrrlQuestion.responseType = ChatResponseTypeMultipleChoice;
                                                            sqrrlQuestion.mcResponseOptions = [json objectForKey:@"choices"];
                                                        } else if ([[json objectForKey:@"_type"] isEqualToString:@"none"]) {
                                                            sqrrlQuestion.responseType = ChatResponseTypeLastQuestion;
                                                        } else if ([[json objectForKey:@"_type"] isEqualToString:@"any_text"]) {
                                                            sqrrlQuestion.responseType = ChatResponseTypeText;
                                                        } else if ([[json objectForKey:@"_type"] isEqualToString:@"number"]) {
                                                            sqrrlQuestion.responseType = ChatResponseTypeNumber;
                                                        }
                                                        
                                                        sqrrlQuestion.questionId = [json objectForKey:@"question_id"];
                                                        
                                                        [self addNewChatBubble:sqrrlQuestion];
                                                        [self setupInputViewWithChat:sqrrlQuestion];
                                                        
                                                    } else {
                                                        SqrrrlChat * response = [[SqrrrlChat alloc] initWithText:[json objectForKey:@"answer_response"]];
                                                        [self addNewChatBubble:response];

                                                        
                                                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                            [self getNewChatBubbleFromServer];
                                                        });
                                                    }
        }];
        
    } else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self getNewChatBubbleFromServer];
        });
    }
    

    
    
}

#pragma mark - UICollectionView Methods


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.chatBubbles.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BaseChat* chatBubble = [self.chatBubbles objectAtIndex:indexPath.item];
    
    if ([chatBubble isKindOfClass:[SqrrrlChat class]]) {
        SqrrrlChatCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SqrrrlChatCollectionViewCell class]) forIndexPath:indexPath];
        cell.chatLabel.text = chatBubble.text;
        
        return cell;
    } else if ([chatBubble isKindOfClass:[UserChat class]]) {
        UserChatCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UserChatCollectionViewCell class]) forIndexPath:indexPath];
        cell.chatLabel.text = chatBubble.text;
        
        return cell;
    }
    
    return nil;

}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.frame.size.width, [BaseChatCollectionViewCell heightWithText:[[self.chatBubbles objectAtIndex:indexPath.item] text]]);
}

#pragma mark - Keyboard Methods

-(void)keyboardWillAppear:(NSNotification*) notification {
    
    NSDictionary * userInfo = notification.userInfo;
    
    CGRect keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];

    self.inputViewBottomConstraint.constant = keyboardSize.size.height - 49;
    
    [UIView animateWithDuration:[[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue] delay:0 options:[[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] floatValue] animations:^{
        self.collectionView.contentInset = UIEdgeInsetsMake(self.collectionView.contentInset.top, self.collectionView.contentInset.left, self.collectionView.contentInset.bottom + keyboardSize.size.height, self.collectionView.contentInset.right);
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.chatBubbles.count-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)keyboardWillHide:(NSNotification*) notification {
 
    NSDictionary * userInfo = notification.userInfo;
    
    CGRect keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.inputViewBottomConstraint.constant = 0;
    
    [UIView animateWithDuration:[[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue] delay:0 options:[[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] floatValue] animations:^{
        self.collectionView.contentInset = UIEdgeInsetsMake(self.collectionView.contentInset.top, self.collectionView.contentInset.left, self.collectionView.contentInset.bottom - keyboardSize.size.height, self.collectionView.contentInset.right);
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.chatBubbles.count-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

@end
