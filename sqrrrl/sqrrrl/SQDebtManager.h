//
//  SQDebtManager.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class SQDebt;


@interface SQDebtManager : NSObject

+ (SQDebtManager*)debtManager;

- (void)loadDebts;
- (NSArray*)debts;
- (void)saveDebts;
- (void)addDebt:(SQDebt*)debt;
- (void)updateDebt:(SQDebt*)debt;
- (BOOL)deleteDebt:(SQDebt*)debt;
- (CGFloat)totalDebtForXDaysAgo:(NSInteger)daysAgo;
- (CGFloat)highestDebtInRecordedHistory;

- (void)accelerateTimeByOneDay;

@end
