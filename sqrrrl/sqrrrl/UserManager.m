//
//  UserManager.m
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-22.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "UserManager.h"

static UserManager * sharedUserManager;

@implementation UserManager

+(UserManager *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUserManager = [[UserManager alloc] init];
    });
    return sharedUserManager;
}

-(void)createUserWithJson:(NSDictionary*)userDictionary {
    
    if ([userDictionary objectForKey:@"age"] && [userDictionary objectForKey:@"age"] != [NSNull null]) {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        self.age = [formatter numberFromString:[userDictionary objectForKey:@"age"]];
    }
    
    if ([userDictionary objectForKey:@"income"] && [userDictionary objectForKey:@"income"] != [NSNull null]) {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        self.income = [formatter numberFromString:[userDictionary objectForKey:@"income"]];
    }
    
    if ([userDictionary objectForKey:@"name"] && [userDictionary objectForKey:@"name"] != [NSNull null]) {
        self.name = [userDictionary objectForKey:@"name"];
    }
    
    if ([userDictionary objectForKey:@"province"] && [userDictionary objectForKey:@"province"] != [NSNull null]) {
        self.province = [userDictionary objectForKey:@"province"];
    }
    
    if ([userDictionary objectForKey:@"categories"] && [userDictionary objectForKey:@"categories"] != [NSNull null]) {
        self.categories = [userDictionary objectForKey:@"categories"];
    }
    
    self.hasEmptyFields = !self.name || !self.province || !self.age || !self.income;
    
}

@end
