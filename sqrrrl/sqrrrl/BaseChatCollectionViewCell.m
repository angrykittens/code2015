//
//  BaseChatCollectionViewCell.m
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "BaseChatCollectionViewCell.h"

@implementation BaseChatCollectionViewCell


+(CGFloat)heightWithText:(NSString*)text {
    
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat verticalPadding = 10;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:17]};
    CGRect rect = [text boundingRectWithSize:CGSizeMake(screenWidth - 110 , CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:attributes
                                     context:nil];
    
    CGFloat returnHeight = rect.size.height + 2*verticalPadding + 5; //5 offset because sometimes the numbers are off by a bit
    return returnHeight < 50.0f ? 50.0f : returnHeight;
}

@end
