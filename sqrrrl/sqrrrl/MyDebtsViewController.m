//
//  MyDebtsViewController.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "MyDebtsViewController.h"
#import "JBChartView.h"
#import "JBBarChartView.h"
#import "SQDebug.h"
#import "SQDebtManager.h"
#import "MyDebtTableViewCell.h"
#import "AddDebtViewController.h"
#import "Dispatch.h"
#import "UIColor+SQColours.h"
#import "UIFont+SQFont.h"
#import "NSDate+Extensions.h"

#import "SQTutorialViewController.h"


@interface MyDebtsViewController () <JBBarChartViewDataSource, JBBarChartViewDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) IBOutlet JBBarChartView *chartView;
@property (nonatomic) IBOutlet UITableView *tableViewDebts;
@property (nonatomic) IBOutlet UILabel *labelTopMarker;
@property (nonatomic) IBOutlet UILabel *labelMidMarker;
@property (nonatomic) IBOutlet UILabel *labelDailyDebt;
@property (nonatomic) IBOutlet UIButton *buttonAddDebt;
@property (nonatomic) SQDebt *debtToEdit;

@property (nonatomic) IBOutlet UILabel *labelDay0;
@property (nonatomic) IBOutlet UILabel *labelDay1;
@property (nonatomic) IBOutlet UILabel *labelDay2;
@property (nonatomic) IBOutlet UILabel *labelDay3;
@property (nonatomic) IBOutlet UILabel *labelDay4;
@property (nonatomic) IBOutlet UILabel *labelDay5;
@property (nonatomic) IBOutlet UILabel *labelDay6;

@end


@implementation MyDebtsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    SQAssertNotNil(self.chartView);

    self.chartView.dataSource = self;
    self.chartView.delegate = self;
    
    self.labelDailyDebt.text = nil;
    
    self.buttonAddDebt.layer.cornerRadius = 3.0f;

    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableViewDebts setTableFooterView:tableFooterView];

    UILabel *tableHeaderView = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableViewDebts.bounds.size.width, 30.0f)];
    tableHeaderView.text = @"My Debts";
    tableHeaderView.font = [UIFont sqFontLightOfSize:14.0f];
    tableHeaderView.textAlignment = NSTextAlignmentCenter;
    tableHeaderView.textColor = [UIColor whiteColor];
    self.tableViewDebts.tableHeaderView = tableHeaderView;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.chartView.alpha = 0.0f;    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadTheChartViewAndTableView];
}

- (void)updateDayLabels
{
    NSDate *day = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE"];
    self.labelDay0.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay1.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay2.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay3.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay4.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay5.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay6.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
}

- (void)reloadTheChartViewAndTableView
{
    [self updateDayLabels];
    
    self.chartView.alpha = 0.0f;

    CGFloat chartMaxScale = [[SQDebtManager debtManager] highestDebtInRecordedHistory];
    self.chartView.minimumValue = 0.0f;
    self.chartView.maximumValue = chartMaxScale;

    self.labelTopMarker.text = [NSString stringWithFormat:@"$%0.0f", chartMaxScale];
    self.labelMidMarker.text = [NSString stringWithFormat:@"$%0.0f", chartMaxScale/2.0f];

    [self.chartView reloadData];

    [self.tableViewDebts reloadData];

    [UIView animateWithDuration:0.3f
                     animations:^{
         self.chartView.alpha = 1.0f;
     }];

}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AddDebtViewController *addDebtViewController = (AddDebtViewController *)segue.destinationViewController;
    if ([segue.identifier isEqualToString: @"AddDebtSegue"]) {
        if (self.debtToEdit) {
            [addDebtViewController editDebt:self.debtToEdit];
        } else {
            [addDebtViewController addDebt];
        }
    }
}

#pragma mark - <JBChartViewDelegate>

- (BOOL)shouldExtendSelectionViewIntoHeaderPaddingForChartView:(JBChartView *)chartView
{
    return NO;
}

- (BOOL)shouldExtendSelectionViewIntoFooterPaddingForChartView:(JBChartView *)chartView
{
    return NO;
}

#pragma mark - JBBarChartViewDataSource

- (NSUInteger)numberOfBarsInBarChartView:(JBBarChartView *)barChartView
{
    return 7;
}

#pragma mark - JBBarChartViewDelegate

- (CGFloat)barChartView:(JBBarChartView *)barChartView heightForBarViewAtIndex:(NSUInteger)index
{
    NSInteger day = [self numberOfBarsInBarChartView:barChartView] - 1 - index;
    CGFloat debtForDay = [[SQDebtManager debtManager] totalDebtForXDaysAgo:day];
    return debtForDay;
}

- (void)barChartView:(JBBarChartView *)barChartView didSelectBarAtIndex:(NSUInteger)index touchPoint:(CGPoint)touchPoint
{
}

- (void)barChartView:(JBBarChartView *)barChartView didSelectBarAtIndex:(NSUInteger)index
{
    NSInteger day = [self numberOfBarsInBarChartView:barChartView] - 1 - index;
    CGFloat debtForDay = [[SQDebtManager debtManager] totalDebtForXDaysAgo:day];
    self.labelDailyDebt.text = [NSString stringWithFormat:@"$%0.2f", debtForDay];

    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.labelDailyDebt.alpha = 1.0f;
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.6f
                                               delay:4.0f
                                             options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                                          animations:^{
                                              self.labelDailyDebt.alpha = 0.0f;
                                          } completion:nil];
                     }];
    
}

- (void)didDeselectBarChartView:(JBBarChartView *)barChartView
{
}

- (UIColor *) barChartView:(JBBarChartView *)barChartView
    colorForBarViewAtIndex:(NSUInteger)index
{
    return [UIColor sqDebtRedColour];
}

- (UIColor *)barSelectionColorForBarChartView:(JBBarChartView *)barChartView
{
    return [UIColor sqYellowColour];
}

//- (CGFloat)barPaddingForBarChartView:(JBBarChartView *)barChartView
//{
//    return 15.0f;
//}

#pragma mark - UITableviewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arrayDebts = [[SQDebtManager debtManager] debts];
    NSInteger numberOfDebts = arrayDebts.count;
    return numberOfDebts + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arrayDebts = [[SQDebtManager debtManager] debts];
    NSInteger numberOfDebts = arrayDebts.count;
    if (indexPath.row == numberOfDebts) {
        return 60.0f;
    }
    return 88.0f;
}

#pragma mark - UITableviewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arrayDebts = [[SQDebtManager debtManager] debts];
    NSInteger numberOfDebts = arrayDebts.count;
    
    if (indexPath.row == numberOfDebts) {
        UITableViewCell *cell =
            [self.tableViewDebts dequeueReusableCellWithIdentifier:@"AddDebtTableViewCell"];
        return cell;
    }

    MyDebtTableViewCell *cell = (MyDebtTableViewCell *)
                                [self.tableViewDebts dequeueReusableCellWithIdentifier:@"MyDebtTableViewCell"];

    SQAssertNotNil(cell);

    SQDebt *debt = [arrayDebts objectAtIndex:indexPath.row];

    SQAssertNotNil(debt);

    [cell populateWithDebt:debt];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableViewDebts deselectRowAtIndexPath:indexPath animated:NO];

    NSArray *arrayDebts = [[SQDebtManager debtManager] debts];
    
    if (indexPath.row == arrayDebts.count) {
        self.debtToEdit = nil;
        [self performSegueWithIdentifier:@"AddDebtSegue" sender:nil];
    }

    else if (arrayDebts.count > indexPath.row) {
        self.debtToEdit = [arrayDebts objectAtIndex:indexPath.row];
        if (self.debtToEdit) {
            [self performSegueWithIdentifier:@"AddDebtSegue" sender:nil];
        }
    }

}

#pragma mark - IBActions

- (IBAction)actionSelectAddDebt:(id)sender
{
    self.debtToEdit = nil;
    [self performSegueWithIdentifier:@"AddDebtSegue" sender:nil];
}

- (IBAction)unwindToMyDebtsViewController:(UIStoryboardSegue *)segue
{
    SQTrace;
}

@end
