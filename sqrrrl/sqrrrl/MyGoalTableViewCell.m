//
//  GoalTableViewCell.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "MyGoalTableViewCell.h"

@implementation MyGoalTableViewCell

- (void)awakeFromNib
{
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];
}


- (void)populateWithGoal:(SQGoal *)goal
{
    self.labelGoalName.text = goal.goalName;
    self.labelGoalAmount.text = [NSString stringWithFormat:@"$%0.0f of $%0.0f", [goal currentAmount], [goal goalTarget]];
}

@end
