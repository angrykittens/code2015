//
//  BaseChatCollectionViewCell.h
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseChatCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel * chatLabel;

+(CGFloat)heightWithText:(NSString*)text;

@end
