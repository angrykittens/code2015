#import "SQDivider.h"


@implementation SQDivider


- (void)awakeFromNib
{    
    [super awakeFromNib];
    
    [self resizeDividerForRetina];
}

- (void)resizeDividerForRetina
{
    NSAssert(self.layoutConstraintWidth != nil ||
             self.layoutConstraintHeight != nil, @"Dividers require one constraint to be connected to function.");
    
    self.layoutConstraintHeight.constant = 0.5f;
    self.layoutConstraintWidth.constant = 0.5f;
}

@end
