//
//  Dispatch.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dispatch : NSObject
+ (void)after:(NSTimeInterval)delay block:(void (^)(void))block;
@end
