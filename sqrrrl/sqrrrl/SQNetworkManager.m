//
//  SQNetworkManager.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQNetworkManager.h"

#define BASE_URL @"http://api.sqrrrl.com"

typedef NS_ENUM (NSInteger, RequestType) {
    GET,
    POST,
    DELETE,
    PUT
};

@implementation SQNetworkManager

- (void)makeGetRequestWithEndpoint:(NSString *)endpoint
                     responseBlock:(SQNetworkResponseBlock)responseBlock
{
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.HTTPAdditionalHeaders = @{ @"Content-Type"  : @"application/json",
                                      @"Accept"  : @"application/json"};
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:endpoint]];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSString *jsonDataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"GET Received: %@", jsonDataString);
        
        NSError *jsonError;
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
        
        if (![NSJSONSerialization isValidJSONObject:jsonDictionary] || jsonError) {
            NSLog(@"Warning: invalid returned JSON");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([(NSHTTPURLResponse *)response statusCode] != 200) {
                NSError *networkError = [NSError errorWithDomain:@"network" code:[(NSHTTPURLResponse *)response statusCode] userInfo:jsonDictionary];
                responseBlock(nil, networkError);
            } else {
                responseBlock(jsonDictionary, error);
            }
        });
        
    }];
    
    [dataTask resume];
}

- (void)makePostRequestWithEndpoint:(NSString *)endpoint
                               json:(NSDictionary *)jsonDictionary
                      responseBlock:(SQNetworkResponseBlock)responseBlock
{
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.HTTPAdditionalHeaders = @{ @"Content-Type"  : @"application/json",
                                      @"Accept"  : @"application/json"};
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:endpoint]];
    [request setHTTPMethod:@"POST"];
    
    if ([NSJSONSerialization isValidJSONObject:jsonDictionary]) {
        NSError *jsonError;
        NSData *requestData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&jsonError];
        
        [request setHTTPBody:requestData];
        
        NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
            NSString *jsonDataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"POST Received: %@", jsonDataString);
            
            NSError *jsonError;
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            
            if (![NSJSONSerialization isValidJSONObject:jsonDictionary] || jsonError) {
                NSLog(@"Warning: invalid returned JSON");
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([(NSHTTPURLResponse *)response statusCode] != 200) {
                    NSError *networkError = [NSError errorWithDomain:@"network" code:[(NSHTTPURLResponse *)response statusCode] userInfo:jsonDictionary];
                    responseBlock(nil, networkError);
                } else {
                    responseBlock(jsonDictionary, error);
                }
                
            });
            
        }];
        
        [postTask resume];
        
    } else {
        
        NSLog(@"Warning: Invalid json object for POST");
    }
    
}

- (void)makeRequestWithType:(RequestType)type
                   endpoint:(NSString *)endpointString
                    andJSON:(NSDictionary *)jsonDictionary
              responseBlock:(SQNetworkResponseBlock)responseBlock
{
    endpointString = [NSString stringWithFormat:@"%@/%@", BASE_URL, endpointString];
    
    if (type == GET) {
        [self makeGetRequestWithEndpoint:endpointString responseBlock:responseBlock];
    } else if (type == POST) {
        [self makePostRequestWithEndpoint:endpointString json:jsonDictionary responseBlock:responseBlock];
    }
    
}

- (void)retrieveNextQuestionWithResponseBlock:(SQNetworkResponseBlock)responseBlock
{
    [self makeRequestWithType:GET
                     endpoint:@"kids"
                      andJSON:nil
                responseBlock: ^(NSDictionary *json, NSError *error) {
                    
                    if (error) {
                        responseBlock(nil, error);
                    } else {
                        
                        NSMutableArray *arrayChildren = [NSMutableArray array];
                        
                        for (NSDictionary * dictionaryQuestions in [json objectForKey : @"questions"]) {
                            if (dictionaryQuestions) {
                                
                            }
                        }
                        
                        responseBlock([arrayChildren copy], nil);
                    }
                }];
    
}

- (void)makeDeleteRequestWithEndpoint:(NSString *)endpoint
                        responseBlock:(SQNetworkResponseBlock)responseBlock
{
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.HTTPAdditionalHeaders = @{ @"Content-Type"  : @"application/json",
                                      @"Accept"  : @"application/json"};
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:endpoint]];
    [request setHTTPMethod:@"DELETE"];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSString *jsonDataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"DELETE Received: %@", jsonDataString);
        
        NSError *jsonError;
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
        
        if (![NSJSONSerialization isValidJSONObject:jsonDictionary] || jsonError) {
            NSLog(@"Warning: invalid returned JSON");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([(NSHTTPURLResponse *)response statusCode] != 200) {
                NSError *networkError = [NSError errorWithDomain:@"network" code:[(NSHTTPURLResponse *)response statusCode] userInfo:jsonDictionary];
                responseBlock(nil, networkError);
            } else {
                responseBlock(jsonDictionary, error);
            }
        });
        
    }];
    
    [dataTask resume];
}

@end
