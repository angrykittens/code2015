//
//  AddGoalViewController.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQGoal.h"

@interface AddGoalViewController : UIViewController

- (void)addGoal;
- (void)editGoal:(SQGoal*)goalToEdit;

@end
