//
//  SQDebt.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SQDebt : NSObject

@property (nonatomic) NSString* debtId;
@property (nonatomic) NSString* debtName;
@property (nonatomic) NSMutableArray* arrayDebtSevenDays;
@property (nonatomic) NSDate* dateLastUpdated;
@property CGFloat debtInterestRate;

+ (SQDebt*)debt;

- (void)update;
- (void)shiftDebtByOneDay;
- (void)setDebtAmount:(CGFloat)debtAmount;
- (CGFloat)currentAmount;

@end
