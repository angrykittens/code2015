//
//  SQ.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface SQUtil : NSObject

+ (id)loadClassObjectFromXib:(Class)clazz;
+ (id)loadObjectWithClass:(Class)objectClass fromXIB:(NSString *)xib withOwner:(id)owner;
+ (UIView *)loadViewWithTag:(int)tag fromXibNamed:(NSString *)xib withOwner:(id)owner;

@end
