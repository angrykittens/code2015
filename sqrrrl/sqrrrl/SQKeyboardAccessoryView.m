//
//  SQKeyboardAccessoryView.m
//

#import "SQKeyboardAccessoryView.h"
#import "SQDebug.h"


@interface SQKeyboardAccessoryView ()

@property (weak, nonatomic) IBOutlet UIButton *buttonDone;
@property (weak, nonatomic) IBOutlet UIButton *buttonPrev;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;

@end

@implementation SQKeyboardAccessoryView

+ (CGFloat)heightOfAccessoryView
{
    return 40.0f;
}

- (void)updateAccessoryView
{
    SQAssert(self.delegateKeyboardAccessory != nil, @"Must have a delegate for the accessory view");
    self.buttonNext.enabled = [self.delegateKeyboardAccessory canSelectNext];
    self.buttonPrev.enabled = [self.delegateKeyboardAccessory canSelectPrevious];
}

- (void)hidePreviousNextButtons
{
    self.buttonNext.hidden = YES;
    self.buttonPrev.hidden = YES;
}

#pragma mark - IBActions

- (IBAction)actionPreviousPressed:(id)sender
{
    SQAssert(self.delegateKeyboardAccessory != nil, @"Must have a delegate for the accessory view");
    [self.delegateKeyboardAccessory didSelectPrevious];
}

- (IBAction)actionNextPressed:(id)sender
{
    SQAssert(self.delegateKeyboardAccessory != nil, @"Must have a delegate for the accessory view");
    [self.delegateKeyboardAccessory didSelectNext];
}

- (IBAction)actionDonePressed:(id)sender
{
    SQAssert(self.delegateKeyboardAccessory != nil, @"Must have a delegate for the accessory view");
    [self.delegateKeyboardAccessory didSelectDone];
}

@end
