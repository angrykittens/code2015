//
//  SQTabBarController.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-22.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQTabBarController.h"

@interface SQTabBarController ()

@end

@implementation SQTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectorActive:) name:@"daily" object:nil];
}

- (void)selectorActive:(NSNotification*)notification
{
    self.selectedIndex = 0;
}

@end
