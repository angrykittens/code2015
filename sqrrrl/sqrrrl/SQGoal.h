//
//  SQGoal.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SQGoal : NSObject

@property (nonatomic) NSString* goalId;
@property (nonatomic) NSString* goalName;
@property (nonatomic) CGFloat goalTarget;
@property (nonatomic) NSMutableArray* arrayGoalSevenDays;
@property (nonatomic) NSDate* dateLastUpdated;

+ (SQGoal*)goal;

- (void)update;
- (void)shiftGoalByOneDay;
- (void)setGoalAmount:(CGFloat)goalAmount;
- (CGFloat)currentAmount;

@end
