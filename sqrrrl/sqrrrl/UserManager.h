//
//  UserManager.h
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-22.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserManager : NSObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSNumber * income;
@property (nonatomic, strong) NSNumber * age;
@property (nonatomic, strong) NSString * province;

@property (nonatomic, strong) NSArray * categories;

@property BOOL hasEmptyFields;

+(UserManager *)sharedInstance;

-(void)createUserWithJson:(NSDictionary*)userDictionary;

@end
