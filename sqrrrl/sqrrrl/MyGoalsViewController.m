//
//  MyGoalsViewController.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "MyGoalsViewController.h"
#import "JBChartView.h"
#import "JBBarChartView.h"
#import "SQDebug.h"
#import "SQGoalManager.h"
#import "MyGoalTableViewCell.h"
#import "AddGoalViewController.h"
#import "Dispatch.h"
#import "UIColor+SQColours.h"
#import "UIFont+SQFont.h"
#import "NSDate+Extensions.h"

#import "SQTutorialViewController.h"


@interface MyGoalsViewController () <JBBarChartViewDataSource, JBBarChartViewDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) IBOutlet JBBarChartView *chartView;
@property (nonatomic) IBOutlet UITableView *tableViewGoals;
@property (nonatomic) IBOutlet UILabel *labelTopMarker;
@property (nonatomic) IBOutlet UILabel *labelMidMarker;
@property (nonatomic) IBOutlet UILabel *labelDailyGoal;
@property (nonatomic) IBOutlet UIButton *buttonAddGoal;
@property (nonatomic) SQGoal *goalToEdit;

@property (nonatomic) IBOutlet UILabel *labelDay0;
@property (nonatomic) IBOutlet UILabel *labelDay1;
@property (nonatomic) IBOutlet UILabel *labelDay2;
@property (nonatomic) IBOutlet UILabel *labelDay3;
@property (nonatomic) IBOutlet UILabel *labelDay4;
@property (nonatomic) IBOutlet UILabel *labelDay5;
@property (nonatomic) IBOutlet UILabel *labelDay6;

@end


@implementation MyGoalsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    SQAssertNotNil(self.chartView);

    self.chartView.dataSource = self;
    self.chartView.delegate = self;
    
    self.labelDailyGoal.text = nil;
    
    self.buttonAddGoal.layer.cornerRadius = 3.0f;

    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableViewGoals setTableFooterView:tableFooterView];

    UILabel *tableHeaderView = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableViewGoals.bounds.size.width, 30.0f)];
    tableHeaderView.text = @"My Goals";
    tableHeaderView.font = [UIFont sqFontLightOfSize:14.0f];
    tableHeaderView.textAlignment = NSTextAlignmentCenter;
    tableHeaderView.textColor = [UIColor whiteColor];
    self.tableViewGoals.tableHeaderView = tableHeaderView;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.chartView.alpha = 0.0f;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadTheChartViewAndTableView];
}

- (void)updateDayLabels
{
    NSDate *day = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE"];
    self.labelDay0.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay1.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay2.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay3.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay4.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay5.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
    self.labelDay6.text = [[dateFormatter stringFromDate:day] uppercaseString];
    day = [day previousDay];
}

- (void)reloadTheChartViewAndTableView
{
    [self updateDayLabels];
    
    self.chartView.alpha = 0.0f;

    CGFloat chartMaxScale = [[SQGoalManager goalManager] highestSumOfAllGoalTargets];
    self.chartView.minimumValue = 0.0f;
    self.chartView.maximumValue = chartMaxScale;

    self.labelTopMarker.text = [NSString stringWithFormat:@"$%0.0f", chartMaxScale];
    self.labelMidMarker.text = [NSString stringWithFormat:@"$%0.0f", chartMaxScale/2.0f];

    [self.chartView reloadData];

    [self.tableViewGoals reloadData];

    [UIView animateWithDuration:0.3f
                     animations:^{
         self.chartView.alpha = 1.0f;
     }];

}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AddGoalViewController *addGoalViewController = (AddGoalViewController *)segue.destinationViewController;
    if ([segue.identifier isEqualToString: @"AddGoalSegue"]) {
        if (self.goalToEdit) {
            [addGoalViewController editGoal:self.goalToEdit];
        } else {
            [addGoalViewController addGoal];
        }
    }
}

#pragma mark - <JBChartViewDelegate>

- (BOOL)shouldExtendSelectionViewIntoHeaderPaddingForChartView:(JBChartView *)chartView
{
    return NO;
}

- (BOOL)shouldExtendSelectionViewIntoFooterPaddingForChartView:(JBChartView *)chartView
{
    return NO;
}

#pragma mark - JBBarChartViewDataSource

- (NSUInteger)numberOfBarsInBarChartView:(JBBarChartView *)barChartView
{
    return 7;
}

#pragma mark - JBBarChartViewDelegate

- (CGFloat)barChartView:(JBBarChartView *)barChartView heightForBarViewAtIndex:(NSUInteger)index
{
    NSInteger day = [self numberOfBarsInBarChartView:barChartView] - 1 - index;
    CGFloat goalForDay = [[SQGoalManager goalManager] totalGoalForXDaysAgo:day];
    return goalForDay;
}

- (void)barChartView:(JBBarChartView *)barChartView didSelectBarAtIndex:(NSUInteger)index touchPoint:(CGPoint)touchPoint
{
}

- (void)barChartView:(JBBarChartView *)barChartView didSelectBarAtIndex:(NSUInteger)index
{
    NSInteger day = [self numberOfBarsInBarChartView:barChartView] - 1 - index;
    CGFloat goalForDay = [[SQGoalManager goalManager] totalGoalForXDaysAgo:day];
    self.labelDailyGoal.text = [NSString stringWithFormat:@"$%0.2f", goalForDay];

    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.labelDailyGoal.alpha = 1.0f;
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.6f
                                               delay:4.0f
                                             options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState
                                          animations:^{
                                              self.labelDailyGoal.alpha = 0.0f;
                                          } completion:nil];
                     }];
    
}

- (void)didDeselectBarChartView:(JBBarChartView *)barChartView
{
}

- (UIColor *) barChartView:(JBBarChartView *)barChartView
    colorForBarViewAtIndex:(NSUInteger)index
{
    return [UIColor sqYellowColour];
}

- (UIColor *)barSelectionColorForBarChartView:(JBBarChartView *)barChartView
{
    return [UIColor sqDarkBlueColour];
}

//- (CGFloat)barPaddingForBarChartView:(JBBarChartView *)barChartView
//{
//    return 15.0f;
//}

#pragma mark - UITableviewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arrayGoals = [[SQGoalManager goalManager] goals];
    NSInteger numberOfGoals = arrayGoals.count;
    return numberOfGoals + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arrayGoals = [[SQGoalManager goalManager] goals];
    NSInteger numberOfGoals = arrayGoals.count;
    if (indexPath.row == numberOfGoals) {
        return 60.0f;
    }
    return 88.0f;
}

#pragma mark - UITableviewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arrayGoals = [[SQGoalManager goalManager] goals];
    NSInteger numberOfGoals = arrayGoals.count;
    
    if (indexPath.row == numberOfGoals) {
        UITableViewCell *cell =
            [self.tableViewGoals dequeueReusableCellWithIdentifier:@"AddGoalTableViewCell"];
        return cell;
    }

    MyGoalTableViewCell *cell = (MyGoalTableViewCell *)
                                [self.tableViewGoals dequeueReusableCellWithIdentifier:@"MyGoalTableViewCell"];

    SQAssertNotNil(cell);

    SQGoal *goal = [arrayGoals objectAtIndex:indexPath.row];

    SQAssertNotNil(goal);

    [cell populateWithGoal:goal];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableViewGoals deselectRowAtIndexPath:indexPath animated:NO];

    NSArray *arrayGoals = [[SQGoalManager goalManager] goals];
    
    if (indexPath.row == arrayGoals.count) {
        self.goalToEdit = nil;
        [self performSegueWithIdentifier:@"AddGoalSegue" sender:nil];
    }

    else if (arrayGoals.count > indexPath.row) {
        self.goalToEdit = [arrayGoals objectAtIndex:indexPath.row];
        if (self.goalToEdit) {
            [self performSegueWithIdentifier:@"AddGoalSegue" sender:nil];
        }
    }

}

#pragma mark - IBActions

- (IBAction)actionSelectAddGoal:(id)sender
{
    self.goalToEdit = nil;
    [self performSegueWithIdentifier:@"AddGoalSegue" sender:nil];
}

- (IBAction)unwindToMyGoalsViewController:(UIStoryboardSegue *)segue
{
    SQTrace;
}

@end
