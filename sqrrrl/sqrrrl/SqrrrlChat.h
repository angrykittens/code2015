//
//  SqrrrlChat.h
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseChat.h"

@interface SqrrrlChat : BaseChat

@property (nonatomic, strong) NSString * correspondingObject;

@end
