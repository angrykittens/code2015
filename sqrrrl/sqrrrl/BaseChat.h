//
//  BaseChat.h
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    ChatResponseTypeMultipleChoice,
    ChatResponseTypeNumber,
    ChatResponseTypeText,
    ChatResponseTypeLastQuestion
}
ChatResponseType;

@interface BaseChat : NSObject

@property (nonatomic, strong) NSString * text;

@property (nonatomic, assign) ChatResponseType responseType;
@property (nonatomic, strong) NSArray * mcResponseOptions;
@property (nonatomic, strong) NSNumber * questionId;


- (instancetype)initWithText:(NSString *)text;

@end
