//
//  AddDebtViewController.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "AddDebtViewController.h"
#import "SQDebtManager.h"
#import "SQUtil.h"
#import "SQKeyboardAccessoryView.h"
#import "SQDebug.h"
#import "UIColor+SQColours.h"
#import "Dispatch.h"


@interface AddDebtViewController () <UITextFieldDelegate, SQKeyboardAccessoryViewDelegate>
@property (nonatomic) SQDebt *debtToEdit;
@property (nonatomic) SQDebt *debtToAdd;
@property (nonatomic) IBOutlet UITextField *textFieldDebtName;
@property (nonatomic) IBOutlet UITextField *textFieldDebtAmount;
@property (nonatomic) NSString *myTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonDone;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeleteDebt;
@property (nonatomic) SQKeyboardAccessoryView *keyboardAccessoryView;

@end

@implementation AddDebtViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.buttonDone.layer.cornerRadius = 3.0f;
    self.buttonDeleteDebt.layer.cornerRadius = 3.0f;

    self.keyboardAccessoryView = [SQUtil loadClassObjectFromXib:[SQKeyboardAccessoryView class]];
    self.keyboardAccessoryView.delegateKeyboardAccessory = self;

    self.textFieldDebtName.inputAccessoryView = self.keyboardAccessoryView;
    self.textFieldDebtAmount.inputAccessoryView = self.keyboardAccessoryView;

    if (self.debtToEdit) {
        self.debtToAdd = nil;
        self.buttonDeleteDebt.hidden = NO;
        self.textFieldDebtName.text = self.debtToEdit.debtName;
        self.textFieldDebtAmount.text = [NSString stringWithFormat:@"$%0.2f", [self.debtToEdit currentAmount]];
        [self.buttonDone setTitle:@"SAVE CHANGES" forState:UIControlStateNormal];
    } else {
        self.debtToAdd = [SQDebt debt];
        self.textFieldDebtName.text = nil;
        self.textFieldDebtAmount.text = nil;
        self.buttonDeleteDebt.hidden = YES;
        [self.buttonDone setTitle:@"ADD NEW DEBT" forState:UIControlStateNormal];
    }

    [self.navigationController.navigationItem setTitle:self.myTitle];
}

- (void)addDebt
{
    self.myTitle = @"Add Debt";
    self.debtToEdit = nil;
}

- (void)editDebt:(SQDebt *)debtToEdit
{
    self.myTitle = @"Edit Debt";
    self.debtToEdit = debtToEdit;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardAccessoryView updateAccessoryView];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.textFieldDebtName resignFirstResponder];
    [self.textFieldDebtAmount resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.textFieldDebtAmount) {
        CGFloat amount = self.textFieldDebtAmount.text.floatValue;
        [self.debtToEdit setDebtAmount:amount];
        [self.debtToAdd setDebtAmount:amount];
        if (amount == 0) {
            self.textFieldDebtAmount.text = @"$0.00";
        } else {
            self.textFieldDebtAmount.text = [NSString stringWithFormat:@"$%0.2f", amount];
        }
    }

    if (textField == self.textFieldDebtName && self.textFieldDebtName.text.length) {
        self.debtToEdit.debtName = self.textFieldDebtName.text;
        self.debtToAdd.debtName = self.textFieldDebtName.text;
    }
}

#pragma mark - Keyboard

- (void)didSelectDone
{
    [self.textFieldDebtName resignFirstResponder];
    [self.textFieldDebtAmount resignFirstResponder];
}

- (BOOL)canSelectPrevious
{
    return [self.textFieldDebtAmount isFirstResponder];
}
- (BOOL)canSelectNext
{
    return [self.textFieldDebtName isFirstResponder];
}

- (void)didSelectPrevious
{
    [self.textFieldDebtName becomeFirstResponder];
}

- (void)didSelectNext
{
    [self.textFieldDebtAmount becomeFirstResponder];
}

#pragma mark - IBActions

- (IBAction)actionSelectCancel:(id)sender
{
//#ifdef DEBUG
//    [[SQDebtManager debtManager] accelerateTimeByOneDay];
//    [[SQDebtManager debtManager] saveDebts];
//#endif

    [[SQDebtManager debtManager] loadDebts];
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)actionSelectDeleteDebt:(id)sender
{
    [[SQDebtManager debtManager] deleteDebt:self.debtToEdit];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionSelectDone:(id)sender
{
    [self.textFieldDebtName resignFirstResponder];
    [self.textFieldDebtAmount resignFirstResponder];
    
    if (self.debtToAdd && self.debtToEdit == nil) {
        [[SQDebtManager debtManager] addDebt:self.debtToAdd];
    } else {
        [[SQDebtManager debtManager] updateDebt:self.debtToEdit];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
