//
//  SQDebtManager.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQDebtManager.h"
#import "SQDebt.h"
#import "SQDebug.h"
#import "FastCoder.h"


static NSString *kDebts = @"debts";


@interface SQDebtManager ()
@property (nonatomic) NSMutableArray *arrayDebts;
@end


@implementation SQDebtManager

+ (SQDebtManager *)debtManager
{
    static SQDebtManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
                      sharedInstance = [[SQDebtManager alloc] init];
                      [sharedInstance loadDebts];
                  });
    return sharedInstance;
}

- (NSMutableArray *)fakeDebts
{
    SQDebt *debt1 = [SQDebt debt];
    debt1.debtName = @"Visa";
    [debt1 setDebtAmount:100.0f];
    SQDebt *debt2 = [SQDebt debt];
    debt2.debtName = @"Mastercard";
    [debt2 setDebtAmount:200.0f];
    return [NSMutableArray arrayWithObjects:debt1, debt2, nil];
}

- (void)loadDebts
{
    NSData *data = [[[NSUserDefaults standardUserDefaults] objectForKey:kDebts] mutableCopy];

    if (data) {
        NSMutableArray *arrayDebts = [FastCoder objectWithData:data];
        self.arrayDebts = arrayDebts;
        SQLog(@"Loaded %d debts from storage", arrayDebts.count);
    }

    //self.arrayDebts = [self fakeDebts];

    if (self.arrayDebts == nil) {
        self.arrayDebts = [NSMutableArray array];
        for (SQDebt *debt in self.arrayDebts) {
            [debt update];
        }
    }
    
}

- (void)saveDebts
{
    if (self.arrayDebts) {
        NSData *data = [FastCoder dataWithRootObject:self.arrayDebts];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:kDebts];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSArray *)debts
{
    if (self.arrayDebts == nil) {
        [self loadDebts];
    }

    return self.arrayDebts;
}

- (void)addDebt:(SQDebt *)debt
{
    if (debt && [debt currentAmount]) {
        if ([self.arrayDebts containsObject:debt] == NO) {
            [self.arrayDebts addObject:debt];
            [self saveDebts];
        }
    }
}

- (BOOL)deleteDebt:(SQDebt *)debt
{
    if (debt) {
        if ([self.arrayDebts containsObject:debt]) {
            [self.arrayDebts removeObject:debt];
            [self saveDebts];
            return YES;
        }
    }
    return NO;
}

- (CGFloat)totalDebtForXDaysAgo:(NSInteger)daysAgo
{
    CGFloat total = 0.0f;
    for (SQDebt *debt in self.arrayDebts) {
        NSNumber *numberDebt = debt.arrayDebtSevenDays[daysAgo];
        if ([numberDebt isKindOfClass:[NSNumber class]]) {
            total += numberDebt.floatValue;
        }
    }
    return total;
}

- (void)updateDebt:(SQDebt *)debt
{
    [self saveDebts];
}

- (CGFloat)highestDebtInRecordedHistory
{
    CGFloat highest = 0.0f;
    for (NSInteger day = 0; day < 7; day++) {
        CGFloat hightestDebt = [self totalDebtForXDaysAgo:day];
        if (hightestDebt > highest) {
            highest = hightestDebt;
        }
    }

    highest = 100.0f * ceilf(highest / 100);
    highest = MAX(100.0f, highest);

    SQLog(@"highest %0.2f", highest);

    return highest;
}

- (void)accelerateTimeByOneDay
{
    SQLog(@"accelerating time ...");
    for (SQDebt *debt in self.arrayDebts) {
        [debt shiftDebtByOneDay];
    }
}

@end
