//
//  SQTutorialView.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-22.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQTutorialViewController.h"
#import "SQUtil.h"
#import "SwipeView.h"
#import "SQTutorialItem.h"
#import "SQDebug.h"
@import CoreGraphics;

@interface SQTutorialViewController () <SwipeViewDelegate, SwipeViewDataSource>
@property (nonatomic) IBOutlet SwipeView *swipeView;
@property (nonatomic) IBOutlet UIImageView *imageViewSquirrel;
@property (nonatomic) IBOutlet UIVisualEffectView *visualEffectView;
@property (nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic) IBOutlet UIButton *buttonNext;
@end

@implementation SQTutorialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.pageControl.numberOfPages = 4;
    self.pageControl.currentPage = 0;
}

+ (UIImage *)snapshotOfView:(UIView *)viewToSnapshot
{
    UIGraphicsBeginImageContextWithOptions(viewToSnapshot.bounds.size, true, 1);
    [viewToSnapshot drawViewHierarchyInRect:viewToSnapshot.bounds afterScreenUpdates:YES];
    UIImage *imageScreenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imageScreenshot;
}

#pragma mark - SwipeView Delegate

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeView.bounds.size;
}

#pragma mark - SwipeView Data Source

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return 4;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    SQTutorialItem *item = [SQUtil loadClassObjectFromXib:[SQTutorialItem class]];

    if (index == 0) {
        [item populateWithImage:[UIImage imageNamed:@"tut0"]
                          title:@"Welcome to Sqrrrl!"
                        andBody:@"Every day, Sqrrrl will help you save money, pay down debts and put you on the road to stress-free Financial Health!"];
    }

    if (index == 1) {
        [item populateWithImage:[UIImage imageNamed:@"tut3"]
                          title:@"Sqrrrl Understands You"
                        andBody:@"Sqrrrl will help you compare your situation with other Canadians using Public Federal Data Sets"];
    }

    if (index == 2) {
        [item populateWithImage:[UIImage imageNamed:@"tut1"]
                          title:@"You're Not Alone!"
                        andBody:@"Check in every day with how much money you saved and get some tips on how you can save even more money!"];
    }

    if (index == 3) {
        [item populateWithImage:[UIImage imageNamed:@"tut2"]
                          title:@"Let's Get Out of Debt!"
                        andBody:@"The first step is to make a list of your outstanding debts, so we can start paying them down and get you back to Healthy Living!"];
    }

    return item;
}

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    if (self.swipeView.currentItemIndex == 3) {
        [self.buttonNext setTitle:@"OK LET'S GO!" forState:UIControlStateNormal];
    } else {
        [self.buttonNext setTitle:@"NEXT" forState:UIControlStateNormal];
    }
    
    self.pageControl.currentPage = swipeView.currentItemIndex;
}

#pragma mark - IBActions

- (IBAction)actionSelectNext:(id)sender
{
    if (self.swipeView.currentItemIndex == 3) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.swipeView scrollByNumberOfItems:1 duration:0.5f];
    }
}

@end
