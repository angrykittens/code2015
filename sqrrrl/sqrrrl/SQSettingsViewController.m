//
//  SQSettingsViewController.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-22.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQSettingsViewController.h"
#import "SQNotificationManager.h"
#import "SQNetworkManager.h"
#import "SQDebug.h"
#import "Dispatch.h"


@interface SQSettingsViewController ()
@property (nonatomic) IBOutlet UIButton *buttonDisableNotifications;
@property (nonatomic) IBOutlet UIButton *buttonClearChat;
@property (nonatomic) IBOutlet UIButton *buttonResetEverything;
@property (nonatomic) SQNetworkManager* networkManager;
@end

@implementation SQSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self updateSettingsView];
}

- (void)updateSettingsView
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"notifications_disabled"]) {
        [self.buttonDisableNotifications setTitle:@"Enable Notifications" forState:UIControlStateNormal];
    } else {
        [self.buttonDisableNotifications setTitle:@"Go Away Sqrrrl" forState:UIControlStateNormal];
    }
}

- (void)clearServerChatData
{
    NSString* vendorId = [[UIDevice currentDevice] identifierForVendor].UUIDString;
    NSString* endpoint = [@"http://sqrrrl.thenoos.net/user/" stringByAppendingString:vendorId];
    self.networkManager = [[SQNetworkManager alloc] init];
    [self.networkManager makeDeleteRequestWithEndpoint:endpoint
                                         responseBlock:^(NSDictionary *json, NSError *error) {
                                         }];
}

- (IBAction)actionSelectClearChat:(id)sender
{
    NSString* vendorId = [[UIDevice currentDevice] identifierForVendor].UUIDString;
    NSString* endpoint = [@"http://sqrrrl.thenoos.net/user/" stringByAppendingString:vendorId];
    self.networkManager = [[SQNetworkManager alloc] init];
    [self.networkManager makeDeleteRequestWithEndpoint:endpoint
                                         responseBlock:^(NSDictionary *json, NSError *error) {
                                             
                                             SQTrace;
                                             
                                             [[[UIAlertView alloc] initWithTitle:@"Chat History Cleared!" message:@"Please relaunch the app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                                             
                                             [Dispatch after:2.0f
                                                       block:^{
                                                           exit(0);
                                                       }];
                                         }];
}

- (IBAction)actionSelectDisableNotifications:(id)sender
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"notifications_disabled"] == YES) {
        // Enable
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"notifications_disabled"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[SQNotificationManager sharedInstance] scheduleDailyReminders];

        [[[UIAlertView alloc] initWithTitle:@"Notifications Enabled!" message:@"Sqrrrl will be ask you every day about your progress." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];

    } else {
        // Disable

        [[UIApplication sharedApplication] cancelAllLocalNotifications];

        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"notifications_disabled"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [[[UIAlertView alloc] initWithTitle:@"Notifications Disabled" message:@"Sqrrrl will be quiet." delegate:nil cancelButtonTitle:@"Thanks" otherButtonTitles: nil] show];
    }

    [self updateSettingsView];
}

- (IBAction)actionSelectClearEverything:(id)sender
{
    [self clearServerChatData];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"tutorial"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"debts"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Goals"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[[UIAlertView alloc] initWithTitle:@"Reset Complete"
                                message:@"Please relaunch the App"
                               delegate:nil
                      cancelButtonTitle:@"Gotcha"
                      otherButtonTitles: nil] show];
    
    [Dispatch after:2.0f
              block:^{
                  exit(0);
              }];
    
}

@end
