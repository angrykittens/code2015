//
//  SQDebt.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQDebt.h"
#import "SQDebtManager.h"

@implementation SQDebt

+ (SQDebt *)debt
{
    SQDebt *debt = [[SQDebt alloc]init];
    CFUUIDRef udid = CFUUIDCreate(NULL);
    debt.debtId = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, udid);
    debt.arrayDebtSevenDays = [NSMutableArray arrayWithObjects:@0.0f,@0.0f,@0.0f,@0.0f,@0.0f,@0.0f,@0.0f,nil];
    debt.dateLastUpdated = [NSDate date];
    return debt;
}

- (void)update
{
    NSInteger numberOfDaysSinceLastUpdate = MIN(7, [self numberOfDaysSinceLastUpdate]);

    for (NSInteger i = 0; i < numberOfDaysSinceLastUpdate; i++) {
        [self shiftDebtByOneDay];
    }
}

- (void)shiftDebtByOneDay
{
    for (NSInteger day = self.arrayDebtSevenDays.count - 1; day >= 1 ; day--) {
        NSNumber *number = [[self.arrayDebtSevenDays objectAtIndex:day-1] copy];
        [self.arrayDebtSevenDays replaceObjectAtIndex:day withObject:number];
    }
}

- (NSInteger)numberOfDaysSinceLastUpdate
{
    NSDate *dateToday = [NSDate date];

    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:dateToday
                                                          toDate:self.dateLastUpdated
                                                         options:0];
    
    self.dateLastUpdated = dateToday;

    return [components day];
}


- (void)setDebtAmount:(CGFloat)debtAmount
{
    NSNumber *numberDebtAmount = [NSNumber numberWithFloat:debtAmount];
    [self.arrayDebtSevenDays replaceObjectAtIndex:0 withObject:numberDebtAmount];
}


- (CGFloat)currentAmount
{
    NSNumber *amount = [self.arrayDebtSevenDays objectAtIndex:0];
    return [amount floatValue];
}

@end
