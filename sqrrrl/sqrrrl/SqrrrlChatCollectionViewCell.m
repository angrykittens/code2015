//
//  ChatCollectionViewCell.m
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SqrrrlChatCollectionViewCell.h"
#import "UIColor+SQColours.h"

@interface SqrrrlChatCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIView *chatBox;

@end

@implementation SqrrrlChatCollectionViewCell

-(void)layoutSubviews {
    [super layoutSubviews];
    self.chatBox.layer.borderColor = [UIColor colorWithWhite:216.0f/255.0f alpha:1.0f].CGColor;
    self.chatBox.layer.borderWidth = 1;
    
}


@end
