#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface SQDivider : UIView

@property (nonatomic) IBOutlet NSLayoutConstraint* layoutConstraintHeight;
@property (nonatomic) IBOutlet NSLayoutConstraint* layoutConstraintWidth;

@end
