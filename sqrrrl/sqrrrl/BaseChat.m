//
//  BaseChat.m
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "BaseChat.h"

@implementation BaseChat

- (instancetype)initWithText:(NSString *)text {
    self = [super init];
    if (self) {
        self.text = text;
    }
    return self;
}

@end
