//
//  AddGoalViewController.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "AddGoalViewController.h"
#import "SQGoalManager.h"
#import "SQUtil.h"
#import "SQKeyboardAccessoryView.h"
#import "SQDebug.h"
#import "UIColor+SQColours.h"
#import "Dispatch.h"


@interface AddGoalViewController () <UITextFieldDelegate, SQKeyboardAccessoryViewDelegate>
@property (nonatomic) SQGoal *goalToEdit;
@property (nonatomic) SQGoal *goalToAdd;
@property (nonatomic) IBOutlet UITextField *textFieldGoalName;
@property (nonatomic) IBOutlet UITextField *textFieldGoalAmount;
@property (nonatomic) IBOutlet UITextField *textFieldGoalTarget;
@property (nonatomic) NSString *myTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonDone;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeleteGoal;
@property (nonatomic) SQKeyboardAccessoryView *keyboardAccessoryView;

@end

@implementation AddGoalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.buttonDone.layer.cornerRadius = 3.0f;
    self.buttonDeleteGoal.layer.cornerRadius = 3.0f;

    self.keyboardAccessoryView = [SQUtil loadClassObjectFromXib:[SQKeyboardAccessoryView class]];
    self.keyboardAccessoryView.delegateKeyboardAccessory = self;

    self.textFieldGoalName.inputAccessoryView = self.keyboardAccessoryView;
    self.textFieldGoalAmount.inputAccessoryView = self.keyboardAccessoryView;
    self.textFieldGoalTarget.inputAccessoryView = self.keyboardAccessoryView;

    if (self.goalToEdit) {
        self.goalToAdd = nil;
        self.buttonDeleteGoal.hidden = NO;
        self.textFieldGoalName.text = self.goalToEdit.goalName;
        self.textFieldGoalAmount.text = [NSString stringWithFormat:@"$%0.2f", [self.goalToEdit currentAmount]];
        self.textFieldGoalTarget.text = [NSString stringWithFormat:@"$%0.2f", [self.goalToEdit goalTarget]];
        [self.buttonDone setTitle:@"SAVE CHANGES" forState:UIControlStateNormal];
    } else {
        self.goalToAdd = [SQGoal goal];
        self.textFieldGoalName.text = nil;
        self.textFieldGoalAmount.text = nil;
        self.textFieldGoalTarget.text = nil;
        self.buttonDeleteGoal.hidden = YES;
        [self.buttonDone setTitle:@"ADD THIS GOAL" forState:UIControlStateNormal];
    }

    [self.navigationController.navigationItem setTitle:self.myTitle];
}

- (void)addGoal
{
    self.myTitle = @"Add Goal";
    self.goalToEdit = nil;
}

- (void)editGoal:(SQGoal *)goalToEdit
{
    self.myTitle = @"Edit Goal";
    self.goalToEdit = goalToEdit;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.keyboardAccessoryView updateAccessoryView];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.textFieldGoalName resignFirstResponder];
    [self.textFieldGoalAmount resignFirstResponder];
    [self.textFieldGoalTarget resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.textFieldGoalAmount) {
        CGFloat amount = self.textFieldGoalAmount.text.floatValue;
        [self.goalToEdit setGoalAmount:amount];
        [self.goalToAdd setGoalAmount:amount];
        if (amount == 0) {
            self.textFieldGoalAmount.text = @"$0.00";
        } else {
            self.textFieldGoalAmount.text = [NSString stringWithFormat:@"$%0.2f", amount];
        }
    }
    
    if (textField == self.textFieldGoalTarget) {
        CGFloat amount = self.textFieldGoalTarget.text.floatValue;
        [self.goalToEdit setGoalTarget:amount];
        [self.goalToAdd setGoalTarget:amount];
        if (amount == 0) {
            self.textFieldGoalTarget.text = @"$0.00";
        } else {
            self.textFieldGoalTarget.text = [NSString stringWithFormat:@"$%0.2f", amount];
        }
    }
    
    if (textField == self.textFieldGoalName && self.textFieldGoalName.text.length) {
        self.goalToEdit.goalName = self.textFieldGoalName.text;
        self.goalToAdd.goalName = self.textFieldGoalName.text;
    }
}

#pragma mark - Keyboard

- (void)didSelectDone
{
    [self.textFieldGoalName resignFirstResponder];
    [self.textFieldGoalAmount resignFirstResponder];
    [self.textFieldGoalTarget resignFirstResponder];
}

- (BOOL)canSelectPrevious
{
    return ![self.textFieldGoalName isFirstResponder];
}
- (BOOL)canSelectNext
{
    return ![self.textFieldGoalTarget isFirstResponder];
}

- (void)didSelectPrevious
{
    if ([self.textFieldGoalTarget isFirstResponder]) {
        [self.textFieldGoalAmount becomeFirstResponder];
    } else if ([self.textFieldGoalAmount isFirstResponder]) {
        [self.textFieldGoalName becomeFirstResponder];
    }
}

- (void)didSelectNext
{
    if ([self.textFieldGoalName isFirstResponder]) {
        [self.textFieldGoalAmount becomeFirstResponder];
    } else if ([self.textFieldGoalAmount isFirstResponder]) {
        [self.textFieldGoalTarget becomeFirstResponder];
    }
}

#pragma mark - IBActions

- (IBAction)actionSelectCancel:(id)sender
{
//#ifdef DEBUG
//    [[SQGoalManager goalManager] accelerateTimeByOneDay];
//    [[SQGoalManager goalManager] saveGoals];
//#endif

    [[SQGoalManager goalManager] loadGoals];
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)actionSelectDeleteGoal:(id)sender
{
    [[SQGoalManager goalManager] deleteGoal:self.goalToEdit];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionSelectDone:(id)sender
{
    [self.textFieldGoalName resignFirstResponder];
    [self.textFieldGoalAmount resignFirstResponder];
    [self.textFieldGoalTarget resignFirstResponder];
    
    if (self.goalToAdd && self.goalToEdit == nil) {
        [[SQGoalManager goalManager] addGoal:self.goalToAdd];
    } else {
        [[SQGoalManager goalManager] updateGoal:self.goalToEdit];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
