//
//  SQ.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQUtil.h"
#import "SQDebug.h"


@implementation SQUtil

+ (id)loadClassObjectFromXib:(Class)clazz
{
    SQAssert(clazz != nil, @"class can not be nil");
    
    id object = [SQUtil loadObjectWithClass:clazz fromXIB:NSStringFromClass(clazz) withOwner:nil];
    SQAssert(object != nil, @"Failed to load object class %@ from Xib %@", NSStringFromClass(clazz), NSStringFromClass(clazz));
    
    return object;
}

+ (id)loadObjectWithClass:(Class)objectClass fromXIB:(NSString *)xib withOwner:(id)owner
{
    NSArray *arrayXibObjects = nil;
    
    @try {
        arrayXibObjects = [[NSBundle mainBundle] loadNibNamed:xib owner:owner options:nil];
    }
    @catch (NSException *e)
    {
        if ([e.name isEqualToString:@"NSUnknownKeyException"]) {
            SQLog(@"The xib for %@ has a broken IB connection %@", xib, e.description);
        }
        else {
            [e raise];
        }
    }
    
    if (arrayXibObjects) {
        for (id object in arrayXibObjects) {
            if ([object isKindOfClass:objectClass]) return object;
        }
    }
    
    SQError(@"Failed to load class %@ from nib file: %@", NSStringFromClass(objectClass), xib);
    
    return nil;
}

+ (UIView *)loadViewWithTag:(int)tag fromXibNamed:(NSString *)xib withOwner:(id)owner
{
    NSArray *arrayXibObjects = [[NSBundle mainBundle] loadNibNamed:xib owner:owner options:nil];
    
    if (arrayXibObjects) {
        for (id object in arrayXibObjects) {
            if ([object isKindOfClass:[UIView class]]) {
                UIView *viewLoading = (UIView *)object;
                if (viewLoading.tag == tag) return object;
            }
        }
    }
    
    SQError(@"Failed to load a view with tag %d from nib file: %@", tag, xib);
    
    return nil;
}

@end
