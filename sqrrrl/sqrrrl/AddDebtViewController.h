//
//  AddDebtViewController.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQDebt.h"

@interface AddDebtViewController : UIViewController

- (void)addDebt;
- (void)editDebt:(SQDebt*)debtToEdit;

@end
