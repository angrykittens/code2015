//
//  UIFont+.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (SQ)

+ (UIFont *)sqFontRegularOfSize:(CGFloat)size;
+ (UIFont *)sqFontLightOfSize:(CGFloat)size;

@end
