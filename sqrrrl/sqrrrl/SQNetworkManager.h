//
//  SQNetworkManager.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SQNetworkResponseBlock)(NSDictionary *json, NSError *error);

@interface SQNetworkManager : NSObject

- (void)makeGetRequestWithEndpoint:(NSString *)endpoint
                     responseBlock:(SQNetworkResponseBlock)responseBlock;

- (void)makePostRequestWithEndpoint:(NSString *)endpoint
                               json:(NSDictionary *)jsonDictionary
                      responseBlock:(SQNetworkResponseBlock)responseBlock;

- (void)makeDeleteRequestWithEndpoint:(NSString *)endpoint
                     responseBlock:(SQNetworkResponseBlock)responseBlock;

@end
