//
//  SQGoal.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQGoal.h"
#import "SQGoalManager.h"

@implementation SQGoal

+ (SQGoal *)goal
{
    SQGoal *goal = [[SQGoal alloc]init];
    CFUUIDRef udid = CFUUIDCreate(NULL);
    goal.goalId = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, udid);
    goal.arrayGoalSevenDays = [NSMutableArray arrayWithObjects:@0.0f,@0.0f,@0.0f,@0.0f,@0.0f,@0.0f,@0.0f,nil];
    goal.dateLastUpdated = [NSDate date];
    return goal;
}

- (void)update
{
    NSInteger numberOfDaysSinceLastUpdate = MIN(7, [self numberOfDaysSinceLastUpdate]);

    for (NSInteger i = 0; i < numberOfDaysSinceLastUpdate; i++) {
        [self shiftGoalByOneDay];
    }
}

- (void)shiftGoalByOneDay
{
    for (NSInteger day = self.arrayGoalSevenDays.count - 1; day >= 1 ; day--) {
        NSNumber *number = [[self.arrayGoalSevenDays objectAtIndex:day-1] copy];
        [self.arrayGoalSevenDays replaceObjectAtIndex:day withObject:number];
    }
}

- (NSInteger)numberOfDaysSinceLastUpdate
{
    NSDate *dateToday = [NSDate date];

    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:dateToday
                                                          toDate:self.dateLastUpdated
                                                         options:0];
    
    self.dateLastUpdated = dateToday;

    return [components day];
}


- (void)setGoalAmount:(CGFloat)goalAmount
{
    NSNumber *numberGoalAmount = [NSNumber numberWithFloat:goalAmount];
    [self.arrayGoalSevenDays replaceObjectAtIndex:0 withObject:numberGoalAmount];
}

- (CGFloat)currentAmount
{
    NSNumber *amount = [self.arrayGoalSevenDays objectAtIndex:0];
    return [amount floatValue];
}

@end
