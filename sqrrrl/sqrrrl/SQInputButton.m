//
//  SQInputButton.m
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQInputButton.h"

@implementation SQInputButton

-(void)representResponse:(NSString *)response {
    [self setTitle:response forState:UIControlStateNormal];
    self.correspondingResponse = response;
}

-(CGSize)intrinsicContentSize {
    CGSize size = [super intrinsicContentSize];
    return CGSizeMake(size.width + 30, size.height);
}

@end
