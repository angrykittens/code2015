//
//  SQTutorialItem.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-22.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQTutorialItem : UIView

- (void)populateWithImage:(UIImage*)image
                    title:(NSString*)title
                  andBody:(NSString*)body;

@end
