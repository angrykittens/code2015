
#import <UIKit/UIKit.h>

@interface SQNotificationManager : NSObject

+ (SQNotificationManager*)sharedInstance;
- (void)scheduleDailyReminders;
- (void)didReceiveLocalNotification:(UILocalNotification *)notification;

@end
