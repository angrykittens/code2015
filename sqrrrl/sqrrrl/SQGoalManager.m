//
//  SQGoalManager.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "SQGoalManager.h"
#import "SQGoal.h"
#import "SQDebug.h"
#import "FastCoder.h"


static NSString *kGoals = @"Goals";


@interface SQGoalManager ()
@property (nonatomic) NSMutableArray *arrayGoals;
@end


@implementation SQGoalManager

+ (SQGoalManager *)goalManager
{
    static SQGoalManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
                      sharedInstance = [[SQGoalManager alloc] init];
                      [sharedInstance loadGoals];
                  });
    return sharedInstance;
}

- (NSMutableArray *)fakeGoals
{
    SQGoal *goal1 = [SQGoal goal];
    goal1.GoalName = @"iPhone";
    [goal1 setGoalAmount:0.0f];
    [goal1 setGoalTarget:600.0f];
    SQGoal *goal2 = [SQGoal goal];
    goal2.GoalName = @"Vacation";
    [goal2 setGoalAmount:0.0f];
    [goal2 setGoalTarget:1200.0f];
    return [NSMutableArray arrayWithObjects:goal1, goal2, nil];
}

- (void)loadGoals
{
    NSData* data = [[[NSUserDefaults standardUserDefaults] objectForKey:kGoals] mutableCopy];
    
    if (data) {
        NSMutableArray *arrayGoals = [FastCoder objectWithData:data];
        self.arrayGoals = arrayGoals;
        SQLog(@"Loaded %d Goals from storage", arrayGoals.count);
    }
    
    //self.arrayGoals = [self fakeGoals];

    if (self.arrayGoals == nil) {
        self.arrayGoals = [NSMutableArray array];

        for (SQGoal *Goal in self.arrayGoals) {
            [Goal update];
        }

    }
}

- (void)saveGoals
{
    if (self.arrayGoals) {
        NSData *data = [FastCoder dataWithRootObject:self.arrayGoals];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:kGoals];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSArray *)goals
{
    if (self.arrayGoals == nil) {
        [self loadGoals];
    }

    return self.arrayGoals;
}

- (void)addGoal:(SQGoal *)goal
{
    if (goal && [goal goalTarget] > 0) {
        if ([self.arrayGoals containsObject:goal] == NO) {
            [self.arrayGoals addObject:goal];
            [self saveGoals];
        }
    }
}

- (BOOL)deleteGoal:(SQGoal *)goal
{
    if (goal) {
        if ([self.arrayGoals containsObject:goal]) {
            [self.arrayGoals removeObject:goal];
            [self saveGoals];
            return YES;
        }
    }
    return NO;
}

- (CGFloat)totalGoalForXDaysAgo:(NSInteger)daysAgo
{
    CGFloat total = 0.0f;
    for (SQGoal *Goal in self.arrayGoals) {
        NSNumber *numberGoal = Goal.arrayGoalSevenDays[daysAgo];
        if ([numberGoal isKindOfClass:[NSNumber class]]) {
            total += numberGoal.floatValue;
        }
    }
    return total;
}

- (void)updateGoal:(SQGoal *)Goal
{
    [self saveGoals];
}

- (CGFloat)highestSumOfAllGoalTargets
{
    CGFloat highest = 0.0f;
    for (SQGoal* goal in self.arrayGoals) {
        highest += goal.goalTarget;
    }
    
    highest = 100.0f * ceilf(highest / 100);
    highest = MAX(100.0f, highest);

    SQLog(@"highest %0.2f", highest);
    
    return highest;
}

- (void)accelerateTimeByOneDay
{
    SQLog(@"accelerating time ...");
    for (SQGoal *Goal in self.arrayGoals) {
        [Goal shiftGoalByOneDay];
    }
}

@end
