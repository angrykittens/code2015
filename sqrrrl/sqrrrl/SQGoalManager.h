//
//  SQGoalManager.h
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class SQGoal;


@interface SQGoalManager : NSObject

+ (SQGoalManager*)goalManager;

- (void)loadGoals;
- (NSArray*)goals;
- (void)saveGoals;
- (void)addGoal:(SQGoal*)Goal;
- (void)updateGoal:(SQGoal*)Goal;
- (BOOL)deleteGoal:(SQGoal*)Goal;
- (CGFloat)totalGoalForXDaysAgo:(NSInteger)daysAgo;
- (CGFloat)highestSumOfAllGoalTargets;

- (void)accelerateTimeByOneDay;

@end
