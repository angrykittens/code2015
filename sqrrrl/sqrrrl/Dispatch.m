//
//  Dispatch.m
//  sqrrrl
//
//  Created by Christopher Larsen on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import "Dispatch.h"

@implementation Dispatch

+ (void)after:(NSTimeInterval)delay block:(void (^)(void))block
{
    int64_t delayInNanoseconds = delay * NSEC_PER_SEC;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInNanoseconds);
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}

@end
