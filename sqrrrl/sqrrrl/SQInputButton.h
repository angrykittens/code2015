//
//  SQInputButton.h
//  sqrrrl
//
//  Created by Alex Christodoulou on 2015-02-21.
//  Copyright (c) 2015 AngryKittens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChat.h"

@interface SQInputButton : UIButton

@property (nonatomic, weak) BaseChat * chat;
@property (nonatomic, weak) NSString * correspondingResponse;

-(void)representResponse:(NSString *)response;

@end
