#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#if DEBUG

#define SQLog(format, ...)          [SQDebug log : __FILE__ lineNumber : __LINE__ function : __PRETTY_FUNCTION__ input : (format), ## __VA_ARGS__]
#define SQTrace                     [SQDebug trace:__PRETTY_FUNCTION__]
#define SQError(format, ...)        [SQDebug error : __FILE__ lineNumber : __LINE__ function : __PRETTY_FUNCTION__ input : (format), ## __VA_ARGS__]
#define SQAssert(eval, format, ...) [SQDebug assert : eval output : __FILE__ lineNumber : __LINE__ function : __PRETTY_FUNCTION__ input : (format), ## __VA_ARGS__]
#define SQAssertNotNil(object)      [SQDebug assert : object output : __FILE__ lineNumber : __LINE__ function : __PRETTY_FUNCTION__]
#define SQFlare                     [SQDebug flare]
#define SQBreakAfter(format)        [SQDebug breakAfter : (format)]
#define SQEnableLogging             [SQDebug disableLogging:NO]
#define SQDisableLogging            [SQDebug disableLogging:YES]
#define SQBreakPoint                kill(getpid(), SIGSTOP)

#else

#define SQTrace
#define SQLog(format, ...)
#define SQError(format, ...)
#define SQAssert(eval, format, ...)
#define SQAssertNotNil(object)
#define SQFlare
#define SQBreakAfter(format)
#define SQEnableLogging
#define SQDisableLogging
#define SQBreakPoint

#endif


@interface SQDebug : NSObject

+ (void)trace:(const char *)stringFunction;
+ (void)log:(char *)fileName lineNumber:(NSInteger)lineNumber function:(const char *)stringFunction input:(NSString *)input, ...;
+ (void)error:(char *)fileName lineNumber:(NSInteger)lineNumber function:(const char *)stringFunction input:(NSString *)input, ...;
+ (void)assert:(NSInteger)evaluate output:(char *)fileName lineNumber:(NSInteger)lineNumber function:(const char *)stringFunction input:(NSString *)input, ...;
+ (void)assert:(id)object output:(char *)fileName lineNumber:(NSInteger)lineNumber function:(const char *)stringFunction;
+ (void)assertNotNil:(NSObject*)evaluate output:(char *)fileName lineNumber:(int)lineNumber function:(const char *)stringFunction;
+ (void)flare;

@end




